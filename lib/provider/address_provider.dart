import 'package:flutter/cupertino.dart';
import 'package:pressy_client/data/model/model.dart';

class MemberAddresses extends ChangeNotifier {
  List<MemberAddress> addresses;

  setAddress(List<MemberAddress> address) {
    this.addresses = address;
    notifyListeners();
  }
}
