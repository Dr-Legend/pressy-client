import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pressy_client/blocs/order/order_bloc.dart';
import 'package:pressy_client/data/model/model.dart';
import 'package:pressy_client/widgets/common/widgets/article_card.dart';

class DataSearch extends SearchDelegate<Article> {
  final List<Article> articles;
  final List<Article> recentArticles = [];
  DataSearch(this.articles);
  @override
  List<Widget> buildActions(BuildContext context) {
    // TODO: implement buildActions
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // TODO: implement buildLeading
    return IconButton(
      icon: AnimatedIcon(
        
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,

      ),
      onPressed: (){
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // TODO: implement buildSuggestions
    final List<Article> suggessionList = query.isEmpty?[]:
        articles.where((article) => article.name.toLowerCase().startsWith(query)).toList();
    print(suggessionList.length);
    return GridView.count(
      padding: EdgeInsets.all(12),
      crossAxisCount: 2,
      crossAxisSpacing: 12,
      mainAxisSpacing: 12,
      shrinkWrap: true,
      childAspectRatio: 0.8,
      children: suggessionList.map((article)=>ArticleCard(article: article,onValueChanged: (value){
        print(value);
        BlocProvider.of<OrderBloc>(context).currentState.copyWith();
      },)).toList(),
    );
  }
}
