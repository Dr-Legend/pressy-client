import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:pressy_client/data/model/model.dart';
import 'package:pressy_client/utils/errors/base_error.dart';

abstract class AuthState extends Equatable {}

class AuthUninitializedState extends AuthState {
  @override
  String toString() => "Auth uninitialized";

  @override
  List<Object> get props => null;
}

class AuthUnauthenticatedState extends AuthState {
  @override
  String toString() => "Unauthenticated";

  @override
  List<Object> get props => null;
}

class AuthLoadingState extends AuthState {
  @override
  String toString() => "Auth loading";

  @override
  List<Object> get props => null;
}

class AuthAuthenticated extends AuthState {
  final AuthCredentials authCredentials;
  final MemberProfile memberProfile;

  AuthAuthenticated(
      {@required this.authCredentials, @required this.memberProfile});

  @override
  String toString() => "Authenticated : ${authCredentials.toJson()}";

  @override
  List<Object> get props => [authCredentials, memberProfile];
}

@immutable
class ForgotPasswordSuccessState extends AuthState {
  ForgotPasswordSuccessState();

  @override
  String toString() => "request sent successfully";

  @override
  List<Object> get props => null;
}

@immutable
class ForgotPasswordFailureState extends AuthState {
  final AppError error;

  ForgotPasswordFailureState({this.error});

  @override
  String toString() => "ForgotPassword failed state: $error";

  @override
  List<Object> get props => [error];
}

@immutable
class ForgotPasswordLoadingState extends AuthState {
  @override
  String toString() => "ForgotPassword loading state";

  @override
  List<Object> get props => null;
}

@immutable
class ForgotPasswordInitialState extends AuthState {
  @override
  String toString() => "Initial State";

  @override
  List<Object> get props => null;
}

@immutable
class ResetPasswordSuccessState extends AuthState {
  ResetPasswordSuccessState();

  @override
  String toString() => "Password reseted successfully";

  @override
  List<Object> get props => null;
}

@immutable
class ResetPasswordFailureState extends AuthState {
  final AppError error;

  ResetPasswordFailureState({this.error});

  @override
  String toString() => "Reset Password failed state: $error";

  @override
  List<Object> get props => [error];
}

@immutable
class ResetPasswordLoadingState extends AuthState {
  @override
  String toString() => "Reset Password loading state";

  @override
  List<Object> get props => null;
}
