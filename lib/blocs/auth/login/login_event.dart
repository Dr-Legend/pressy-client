import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class LoginEvent extends Equatable {
  LoginEvent();
}

class LoginButtonPressedEvent extends LoginEvent {
  final String email;
  final String password;

  LoginButtonPressedEvent({@required this.email, @required this.password})
      : assert(email != null && password != null);

  @override
  String toString() =>
      "Login button pressed, email : $email, password : $password";

  @override
  List<Object> get props => [email, password];
}

class LoginSubmitFormEvent extends LoginEvent {
  final String email;
  final String password;

  LoginSubmitFormEvent({@required this.email, @required this.password});
  @override
  List<Object> get props => [email, password];
  @override
  String toString() =>
      "Login submit form with email : $email and password : $password";
}

class ToggleLoginWidgetEvent extends LoginEvent {
  final bool toggleLoginWidget;

  ToggleLoginWidgetEvent(this.toggleLoginWidget);
  @override
  List<Object> get props => [toggleLoginWidget];
}
