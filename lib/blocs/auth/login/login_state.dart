import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:pressy_client/data/model/errors/api_error.dart';

@immutable
class LoginState {
  final bool isValid;
  final bool isEmailValid;

  final bool toggleLoginWidget;

  LoginState({this.toggleLoginWidget, this.isValid, this.isEmailValid});

  LoginState copyWith({
    bool toggleLoginWidget,
    bool isValid,
    bool isEmailValid,
  }) {
    return LoginState(
      toggleLoginWidget: toggleLoginWidget ?? this.toggleLoginWidget,
      isValid: isValid ?? this.isValid,
      isEmailValid: isEmailValid ?? this.isEmailValid,
    );
  }
}

@immutable
class LoginSuccessState extends LoginState {
  @override
  String toString() => "Login Success";
}

@immutable
class LoginLoadingState extends LoginState {
  final bool isValid;
  final bool isEmailValid;
  final bool toggleLoginWidget;

  LoginLoadingState({this.isValid, this.isEmailValid, this.toggleLoginWidget})
      : super(
          isValid: isValid,
          isEmailValid: isEmailValid,
          toggleLoginWidget: toggleLoginWidget,
        );

  @override
  String toString() => "Login loading";
}

@immutable
class LoginInitialState extends LoginState {
  final bool isValid;
  final bool isEmailValid;
  final bool toggleLoginWidget;

  LoginInitialState({this.isEmailValid, this.toggleLoginWidget, this.isValid})
      : super(
            toggleLoginWidget: toggleLoginWidget,
            isEmailValid: isEmailValid,
            isValid: isValid);

  @override
  String toString() => "Login initial state : is valid : $isValid";
}

@immutable
class LoginFailureState extends LoginState {
  final ApiError error;
  final bool isValid;
  final bool isEmailValid;
  final bool toggleLoginWidget;

  LoginFailureState(
      {@required this.error,
      this.toggleLoginWidget,
      this.isValid,
      this.isEmailValid})
      : super(
            isValid: isValid,
            toggleLoginWidget: toggleLoginWidget,
            isEmailValid: isEmailValid);

  @override
  String toString() => "Login error state: $error";
}
