import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pressy_client/blocs/auth/auth_bloc.dart';
import 'package:pressy_client/blocs/auth/auth_event.dart';
import 'package:pressy_client/blocs/auth/login/login_event.dart';
import 'package:pressy_client/blocs/auth/login/login_state.dart';
import 'package:pressy_client/data/data_source/data_source.dart';
import 'package:pressy_client/data/model/errors/api_error.dart';
import 'package:pressy_client/data/model/model.dart';
import 'package:pressy_client/utils/validators/validators.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthBloc authBloc;
  final IMemberDataSource memberDataSource;
  final IAuthDataSource authDataSource;

  LoginBloc({
    @required this.authBloc,
    @required this.memberDataSource,
    @required this.authDataSource,
  });

  @override
  LoginState get initialState => LoginInitialState(
      isEmailValid: false, isValid: false, toggleLoginWidget: false);

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is ToggleLoginWidgetEvent) {
      yield currentState.copyWith(toggleLoginWidget: event.toggleLoginWidget);
    }

    if (event is LoginSubmitFormEvent) {
      bool isValid = Validators.emailValidator(event.email) == null;
      bool isEmailValid = Validators.emailValidator(event.email) == null;
      isValid &= Validators.loginPasswordValidator(event.password) == null;

      yield currentState.copyWith(isValid: isValid, isEmailValid: isEmailValid);
    }

    if (event is LoginButtonPressedEvent) {
      yield LoginLoadingState(
        toggleLoginWidget: currentState.toggleLoginWidget,
        isValid: currentState.isValid,
        isEmailValid: currentState.isEmailValid,
      );

      try {
        var loginRequest =
            LoginRequestModel(email: event.email, password: event.password);
        var authCredentials = await this.authDataSource.login(loginRequest);

        this
            .authBloc
            .dispatch(AuthLoggedInEvent(authCredentials: authCredentials));

        yield LoginSuccessState();
      } on ApiError catch (error) {
        yield LoginFailureState(
          error: error,
          toggleLoginWidget: currentState.toggleLoginWidget,
          isValid: currentState.isValid,
          isEmailValid: currentState.isEmailValid,
        );
      }
    }
  }
}
