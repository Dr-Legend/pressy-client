import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:pressy_client/data/model/errors/api_error.dart';

@immutable
abstract class SignUpState extends Equatable {
  SignUpState([List props]);
}

@immutable
class SignUpSuccessState extends SignUpState {
  @override
  String toString() => "Sign Up Succeeded";

  @override
  List<Object> get props => null;
}

@immutable
class SignUpFailureState extends SignUpState {
  final ApiError error;

  SignUpFailureState({this.error}) : super([error]);

  @override
  String toString() => "Sign Up failed state: $error";

  @override
  List<Object> get props => [error];
}

@immutable
class SignUpInitialState extends SignUpState {
  final bool isValid;

  SignUpInitialState({this.isValid = false}) : super([isValid]);

  @override
  String toString() => "Sign Up initial state, isValid : $isValid";

  @override
  List<Object> get props => [isValid];
}

@immutable
class SignUpLoadingState extends SignUpState {
  @override
  String toString() => "Sign Up loading state";

  @override
  List<Object> get props => null;
}
