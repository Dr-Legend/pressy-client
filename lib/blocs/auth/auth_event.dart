import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:pressy_client/data/model/model.dart';

abstract class AuthEvent extends Equatable {
  final List props;
  AuthEvent([this.props]);
}

class AuthAppStartedEvent extends AuthEvent {
  @override
  String toString() => "App started";
}

class AuthLoggedInEvent extends AuthEvent {
  final AuthCredentials authCredentials;

  AuthLoggedInEvent({@required this.authCredentials})
      : super([authCredentials]);

  @override
  String toString() => "Logged in event : ${authCredentials.toJson()}";
}

class AuthLoggedOutEvent extends AuthEvent {
  @override
  String toString() => "Logged out event";
}

class ResetPasswordRequestEvent extends AuthEvent {
  final ResetPasswordRequestModel resetPasswordRequestModel;

  ResetPasswordRequestEvent({this.resetPasswordRequestModel});
  @override
  String toString() => "Reset password request even";
}

class ResetPasswordEvent extends AuthEvent {
  final String verificationCode;
  final ResetPasswordModel resetPasswordModel;

  ResetPasswordEvent({this.resetPasswordModel, this.verificationCode});
  @override
  String toString() => "Reset password request even";
}
