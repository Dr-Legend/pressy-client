import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:pressy_client/utils/errors/base_error.dart';

@immutable
abstract class AddPaymentAccountState extends Equatable {}

@immutable
class AddPaymentAccountInputState extends AddPaymentAccountState {
  final bool isInputValid;

  AddPaymentAccountInputState({this.isInputValid = false});

  @override
  List<Object> get props => [isInputValid];
}

@immutable
class AddPaymentAccountErrorState extends AddPaymentAccountState {
  final AppError error;

  AddPaymentAccountErrorState({@required this.error});

  @override
  List<Object> get props => [error];
}

@immutable
class AddPaymentAccountLoadingState extends AddPaymentAccountState {
  @override
  List<Object> get props => null;
}

@immutable
class AddPaymentAccountSuccessState extends AddPaymentAccountState {
  @override
  List<Object> get props => null;
}
