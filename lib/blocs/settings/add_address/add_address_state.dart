import 'package:equatable/equatable.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:meta/meta.dart';
import 'package:pressy_client/data/model/errors/api_error.dart';
import 'package:pressy_client/data/model/model.dart';
import 'package:pressy_client/utils/errors/base_error.dart';

@immutable
abstract class AddAddressState extends Equatable {
  final AppError error;
  final bool isLoading;

  AddAddressState(this.isLoading, this.error);
}

@immutable
class AddAddressInputState extends AddAddressState {
  @override
  final bool isLoading;

  @override
  final AppError error;

  final List<Prediction> predictions;

  AddAddressInputState(
      {@required this.predictions, this.isLoading = false, this.error})
      : super(isLoading, error);

  @override
  List<Object> get props => [predictions, error, isLoading];
}

@immutable
class AddAddressExtraInfoState extends AddAddressState {
  @override
  final AppError error;

  @override
  final bool isLoading;

  final Prediction confirmedPrediction;

  AddAddressExtraInfoState(
      {@required this.confirmedPrediction, this.isLoading = false, this.error})
      : super(false, null);

  @override
  List<Object> get props => [error, isLoading, confirmedPrediction];
}

@immutable
class AddAddressSuccessState extends AddAddressState {
  final List<MemberAddress> address;
  AddAddressSuccessState({@required this.address}) : super(false, null);

  @override
  List<Object> get props => null;
}

@immutable
class AddAddressErrorState extends AddAddressState {
  final AppError error;
  AddAddressErrorState(this.error) : super(false, null);

  @override
  List<Object> get props => null;
}
