import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MemberInfoEvent extends Equatable {
  MemberInfoEvent([List props]);
}

class MemberInfoBeginEditingEvent extends MemberInfoEvent {
  @override
  List<Object> get props => null;
}

class MemberInfoDiscardEditingEvent extends MemberInfoEvent {
  @override
  List<Object> get props => null;
}

class MemberInfoSubmitEvent extends MemberInfoEvent {
  final String firstName;
  final String lastName;
  final String email;
  final String phone;

  MemberInfoSubmitEvent(
      {this.firstName, this.lastName, this.email, this.phone});

  @override
  List<Object> get props => null;
}

class MemberInfoConfirmEditingEvent extends MemberInfoEvent {
  final String firstName;
  final String lastName;
  final String email;
  final String phone;

  MemberInfoConfirmEditingEvent(
      {this.firstName, this.lastName, this.email, this.phone});

  @override
  List<Object> get props => null;
}
