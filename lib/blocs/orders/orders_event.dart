import 'package:equatable/equatable.dart';

abstract class OrdersEvent extends Equatable {
  OrdersEvent([List props]);
}

class LoadOrdersEvent extends OrdersEvent {
  LoadOrdersEvent() : super([]);

  @override
  List<Object> get props => null;
}
