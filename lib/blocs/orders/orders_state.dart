import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:pressy_client/data/model/model.dart';
import 'package:pressy_client/utils/errors/base_error.dart';

abstract class OrdersState extends Equatable {}

class OrdersLoadingState extends OrdersState {
  @override
  List<Object> get props => null;
}

class OrdersErrorState extends OrdersState {
  final AppError error;

  OrdersErrorState({this.error});

  @override
  List<Object> get props => [error];
}

class OrdersReadyState extends OrdersState {
  final List<Order> pastOrders;
  final List<Order> ongoingOrders;
  final List<Order> futureOrders;

  OrdersReadyState({this.pastOrders, this.ongoingOrders, this.futureOrders});

  @override
  List<Object> get props => [pastOrders, ongoingOrders, futureOrders];
}
