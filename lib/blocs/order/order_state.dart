import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:pressy_client/data/model/model.dart';
import 'package:pressy_client/data/model/order/order_builder/order_builder.dart';
import 'package:pressy_client/utils/errors/base_error.dart';

@immutable
class OrderState extends Equatable {
  final OrderRequestBuilder orderRequestBuilder;
  final OrderSlotState pickupSlotState;
  final OrderSlotState deliverySlotState;
  final ArticleState articleState;
  final OrderAddressState addressState;
  final OrderPaymentAccountState paymentAccountState;
  final int step;
  final bool isLoading;
  final bool success;
  final AppError error;
  final String comment;
  //Coupon
  final bool isCouponValid;
  final Coupon coupon;
  final String category;
  final String searchQuery;
  //Order's details
  final double totalPrice;
  final double estimatePrice;
  final OrderType orderType;
  final Map<Article, int> cart;
  OrderState(
      {@required this.orderRequestBuilder,
      this.pickupSlotState,
      this.deliverySlotState,
      this.articleState,
      this.paymentAccountState,
      this.addressState,
      this.comment,
      this.step = 0,
      this.isLoading = false,
      this.success = false,
      this.error,
      this.category,
      this.coupon,
      this.searchQuery,
      this.isCouponValid = false,
      this.totalPrice = 0,
      this.estimatePrice = 0,
      this.orderType,
      this.cart});

  OrderState copyWith({
    OrderRequestBuilder orderRequestBuilder,
    OrderSlotState pickupSlotState,
    OrderSlotState deliverySlotState,
    ArticleState articleState,
    OrderAddressState addressState,
    OrderPaymentAccountState paymentAccountState,
    int step,
    bool isLoading,
    bool success,
    AppError error,
    bool isCouponValid,
    Coupon coupon,
    String category,
    String searchQuery,
    double totalPrice,
    double estimatePrice,
    OrderType orderType,
    Map<Article, int> cart,
  }) =>
      OrderState(
        orderRequestBuilder: orderRequestBuilder ?? this.orderRequestBuilder,
        pickupSlotState: pickupSlotState ?? this.pickupSlotState,
        deliverySlotState: deliverySlotState ?? this.deliverySlotState,
        articleState: articleState ?? this.articleState,
        paymentAccountState: paymentAccountState ?? this.paymentAccountState,
        addressState: addressState ?? this.addressState,
        step: step ?? this.step,
        isLoading: isLoading ?? this.isLoading,
        success: success ?? this.success,
        error: error ?? this.error,
        category: category ?? this.category,
        coupon: coupon ?? this.coupon,
        isCouponValid: isCouponValid ?? this.isCouponValid,
        searchQuery: searchQuery ?? this.searchQuery,
        totalPrice: totalPrice ?? this.totalPrice,
        estimatePrice: estimatePrice ?? this.estimatePrice,
        orderType: orderType ?? this.orderType,
        cart: cart ?? this.cart,
      );

  @override
  List<Object> get props => [
        cart,
        totalPrice,
        estimatePrice,
        totalPrice,
        addressState,
        articleState,
        deliverySlotState,
        orderRequestBuilder,
        pickupSlotState,
        paymentAccountState,
        addressState,
        orderType,
        error,
        searchQuery,
        step,
        success,
        isLoading,
        isCouponValid,
        comment
      ];
}

@immutable
abstract class ArticleState extends Equatable {}

@immutable
abstract class OrderAddressState extends Equatable {}

@immutable
abstract class OrderPaymentAccountState extends Equatable {}

@immutable
abstract class OrderSlotState extends Equatable {}

@immutable
class OrderAddressLoadingState extends OrderAddressState {
  @override
  List<Object> get props => null;
}

@immutable
class OrderPaymentAccountLoadingState extends OrderPaymentAccountState {
  @override
  List<Object> get props => null;
}

@immutable
class OrderSlotLoadingState extends OrderSlotState {
  @override
  List<Object> get props => null;
}

@immutable
class ArticlesLoadingState extends ArticleState {
  @override
  List<Object> get props => null;
}

@immutable
class OrderPaymentAccountReadyState extends OrderPaymentAccountState {
  final List<PaymentAccount> paymentAccounts;

  OrderPaymentAccountReadyState({this.paymentAccounts});

  @override
  List<Object> get props => [paymentAccounts];
}

@immutable
class OrderAddressReadyState extends OrderAddressState {
  final List<MemberAddress> addresses;

  OrderAddressReadyState({this.addresses});

  @override
  List<Object> get props => [addresses];
}

@immutable
class OrderSlotReadyState extends OrderSlotState {
  final bool canMoveForward;
  final List<Slot> slots;

  OrderSlotReadyState({this.slots, this.canMoveForward});
  @override
  String toString() => "Slot ready state Called";
  @override
  List<Object> get props => [canMoveForward, slots];
}

@immutable
class ArticlesReadyState extends ArticleState {
  final Article weightedArticle;
  final List<Article> articles;

  ArticlesReadyState({this.weightedArticle, this.articles});

  @override
  List<Object> get props => [weightedArticle, articles];
}
