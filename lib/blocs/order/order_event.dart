import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:pressy_client/data/model/model.dart';

abstract class OrderEvent extends Equatable {
  OrderEvent([List props]);
}

class FetchOrderDataEvent extends OrderEvent {
  @override
  List<Object> get props => null;
}

class GoToNextStepEvent extends OrderEvent {
  @override
  List<Object> get props => null;
}

class GoToPreviousStepEvent extends OrderEvent {
  @override
  List<Object> get props => null;
}

class ConfirmOrderEvent extends OrderEvent {
  @override
  List<Object> get props => null;
}

class SearchQueryChanged extends OrderEvent {
  final String searchQuery;

  SearchQueryChanged(this.searchQuery) : super([searchQuery]);

  @override
  List<Object> get props => null;
}

class SelectPickupSlotEvent extends OrderEvent {
  final Slot pickupSlot;

  SelectPickupSlotEvent(this.pickupSlot) : super([pickupSlot]);

  @override
  List<Object> get props => null;
}

class SelectDeliverySlotEvent extends OrderEvent {
  final Slot deliverySlot;

  SelectDeliverySlotEvent(this.deliverySlot) : super([deliverySlot]);

  @override
  List<Object> get props => null;
}

class SelectAddressEvent extends OrderEvent {
  final MemberAddress address;

  SelectAddressEvent(this.address) : super([address]);

  @override
  List<Object> get props => null;
}

class FetchAddressEvent extends OrderEvent {
  @override
  List<Object> get props => null;
}

class SelectOrderTypeEvent extends OrderEvent {
  final OrderType orderType;
  final double estimatedPrice;
  final String orderComment;
  final String usedCouponId;
  final bool isCouponApplied;
  final String comment;
  SelectOrderTypeEvent(this.orderType, this.estimatedPrice, this.orderComment,
      {this.usedCouponId, this.isCouponApplied, this.comment});

  @override
  List<Object> get props => null;
}

class SelectPaymentAccountEvent extends OrderEvent {
  final PaymentAccount paymentAccount;

  SelectPaymentAccountEvent(this.paymentAccount) : super([paymentAccount]);

  @override
  List<Object> get props => null;
}

class SaveCouponInOrderStateEvent extends OrderEvent {
  final bool isCouponValid;
  final Coupon coupon;
  final String category;
  SaveCouponInOrderStateEvent({this.isCouponValid, this.coupon, this.category});

  @override
  List<Object> get props => null;
}

class TotalChangedEvent extends OrderEvent {
  final Map<Article, int> cart;

  TotalChangedEvent(this.cart) : super([cart]);

  @override
  List<Object> get props => null;
}

class OrderTypeChangedEvent extends OrderEvent {
  final OrderType orderType;

  OrderTypeChangedEvent(this.orderType) : super([orderType]);

  @override
  List<Object> get props => null;
}

class EstimatePriceChangedEvent extends OrderEvent {
  final Map<Article, int> cart;

  EstimatePriceChangedEvent(this.cart) : super([cart]);

  @override
  List<Object> get props => null;
}

class CartChangedEvent extends OrderEvent {
  final int itemCount;
  final Article article;

  CartChangedEvent(this.article, this.itemCount) : super([article, itemCount]);

  @override
  List<Object> get props => null;
}
