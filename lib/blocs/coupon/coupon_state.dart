import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:pressy_client/data/model/errors/api_error.dart';
import 'package:pressy_client/data/model/model.dart';
import 'package:pressy_client/data/model/order/coupon_builder/coupon_builder.dart';
import 'package:pressy_client/utils/errors/base_error.dart';

@immutable
abstract class CouponState extends Equatable {}

@immutable
class CouponSuccessState extends CouponState {
  final bool isValid;
  final Coupon coupon;
  final String category;
  CouponSuccessState(
      {@required this.isValid, @required this.coupon, @required this.category});
  @override
  String toString() => "Coupon redeemed successfully";

  @override
  List<Object> get props => [isValid, coupon, category];
}

@immutable
class CouponFailureState extends CouponState {
  final AppError error;

  CouponFailureState({this.error});

  @override
  String toString() => "Coupon failed state: $error";

  @override
  List<Object> get props => [error];
}

@immutable
class CouponLoadingState extends CouponState {
  @override
  String toString() => "Coupon loading state";

  @override
  List<Object> get props => null;
}

@immutable
class CouponInitialState extends CouponState {
  @override
  String toString() => "Initial State";

  @override
  List<Object> get props => null;
}
