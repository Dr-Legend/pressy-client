import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:core';

import 'package:pressy_client/data/model/model.dart';
import 'package:pressy_client/utils/style/app_theme.dart';

typedef void onSeleted(Slot slot);

class SlotUI extends StatefulWidget {
  final List<Slot> slots;
  final onSeleted onSlotSelected;
  SlotUI({Key key, this.slots, this.onSlotSelected}) : super(key: key);

  @override
  _SlotUIState createState() => _SlotUIState();
}

class _SlotUIState extends State<SlotUI> {
  Map<String, List<Slot>> formattedSlots = Map<String, List<Slot>>();
  int _selectedDateIndex = 0;
  int _selectedTimeIndex = 0;
  DateFormat _dateFormat = DateFormat("EEEE dd MMM", "fr");
  DateFormat _timeFormat = DateFormat("HH:mm", "fr");
  Color _selectedTextColor = Colors.orange;
  bool isDateSelected = false;
  bool isTimeSelected = false;
  @override
  void initState() {
    formatSlot();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var bottomSheetHeight = (MediaQuery.of(context).size.height / 3) * 1.3;
    final ThemeData themeData = Theme.of(context);
    return Theme(
      data: themeData.copyWith(canvasColor: Colors.white10),
      child: SingleChildScrollView(
        child: Material(
          elevation: 10,
          type: MaterialType.transparency,
          child: Container(
            padding: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
            height: bottomSheetHeight,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Align(
                  alignment: Alignment.bottomRight,
                  child: FlatButton(
                      child: Text(
                        "terminé",
                        style:
                            TextStyle(fontSize: 13, color: ColorPalette.orange),
                      ),
                      onPressed: () {
                        if (formattedSlots.length != 0) {
                          this.widget.onSlotSelected(formattedSlots.entries
                              .elementAt(_selectedDateIndex)
                              .value[_selectedTimeIndex]);
                        }
                        Navigator.pop(context);
                      }),
                ),
                Flexible(
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Flexible(
                          child: ListView.builder(
                            itemCount: formattedSlots.entries.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: ListTile(
                                  title: Container(
                                    height: 75,
                                    child: Text(
                                      "${formattedSlots.entries.elementAt(index).key}",
                                      style: TextStyle(
                                          color: index == _selectedDateIndex
                                              ? _selectedTextColor
                                              : Colors.black,
                                          fontSize: 16,
                                          fontFamily: "Avenir",
                                          fontWeight: FontWeight.w200),
                                    ),
                                  ),
                                  key: UniqueKey(),
                                  onTap: () {
                                    setState(() {
                                      _selectedDateIndex = index;
                                    });
                                  },
                                ),
                              );
                            },
                          ),
                        ),
                        Flexible(
                          child: ListView.builder(
                            itemCount: formattedSlots.entries.length != 0
                                ? formattedSlots.entries
                                    .elementAt(_selectedDateIndex)
                                    .value
                                    .length
                                : 0,
                            itemBuilder: (context, index) {
                              return ListTile(
                                key: UniqueKey(),
                                onTap: () {
                                  setState(() {
                                    _selectedTimeIndex = index;
                                  });
                                  this.widget.onSlotSelected(formattedSlots
                                      .entries
                                      .elementAt(_selectedDateIndex)
                                      .value[_selectedTimeIndex]);
                                },
                                title: Container(
                                  height: 95,
                                  child: Text(
                                    "${_timeFormat.format(formattedSlots.entries.elementAt(_selectedDateIndex).value[index].startDate)} - ${_timeFormat.format(formattedSlots.entries.elementAt(_selectedDateIndex).value[index].startDate.add(Duration(minutes: 30)))}",
                                    style: TextStyle(
                                      fontFamily: "Avenir",
                                      fontSize: 17,
                                      fontWeight: FontWeight.w500,
                                      color: _selectedTimeIndex == index
                                          ? _selectedTextColor
                                          : Colors.black,
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ]),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  formatSlot() {
    for (int j = 0; j < widget.slots.length; j++) {
      List<Slot> startDateList = [];
      for (int i = 0; i < widget.slots.length; i++) {
        if (widget.slots[j].startDate.day == widget.slots[i].startDate.day) {
          //formattedSlots.addEntries(slots[j].startDate,s)
          startDateList.add(widget.slots[i]);
        }
      }
//      formattedSlots[widget.slots[j].startDate] = startDateList;
      formattedSlots.putIfAbsent(
          _dateFormat.format(widget.slots[j].startDate), () => startDateList);
//      if (formattedSlots.keys.length != 0) {
//        for (var key in formattedSlots.keys) {
//          if (key.day != widget.slots[j].startDate.day) {
//            formattedSlots[widget.slots[j].startDate] = startDateList;
//          }
//        }
//      } else {
//        formattedSlots[widget.slots[j].startDate] = startDateList;
//      }
    }
  }
}
