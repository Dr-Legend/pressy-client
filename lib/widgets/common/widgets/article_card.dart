import 'package:flutter/material.dart';
import 'package:pressy_client/data/model/order/article/article.dart';
import 'package:pressy_client/utils/style/app_theme.dart';
import 'package:pressy_client/widgets/order/stepper/numeric_stepper.dart';
import 'package:pressy_client/widgets/order/stepper/old_numertic_stepper.dart';

class ArticleCard extends StatefulWidget {
  final Article article;
  ValueChanged<int> onValueChanged;
  ArticleCard({Key key, this.article, this.onValueChanged}) : super(key: key);
  @override
  _ArticleCardState createState() => _ArticleCardState();
}

class _ArticleCardState extends State<ArticleCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      key: UniqueKey(),
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          border: Border.all(color: ColorPalette.borderGray, width: 1),
          borderRadius: BorderRadius.circular(4),
          color: Colors.white),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: SizedBox(
              height: 120,
              child: Image.network(this.widget.article.photoUrl),
            ),
          ),
          SizedBox(height: 12),
          Text(
            this.widget.article.name,
            style: TextStyle(fontWeight: FontWeight.bold),
            overflow: TextOverflow.ellipsis,
            maxLines: 2,
          ),
          SizedBox(height: 4),
          Text("${this.widget.article.laundryPrice}€",
              style: TextStyle(
                  color: ColorPalette.darkGray, fontWeight: FontWeight.bold)),
          SizedBox(height: 12),
          Expanded(
            child: Container(),
          ),
          NumericStepper(
            onValueChanged: this.widget.onValueChanged,
          ),
        ],
      ),
    );
  }
}
