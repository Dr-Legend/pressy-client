import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pressy_client/blocs/coupon/coupon_bloc.dart';
import 'package:pressy_client/blocs/order/order_bloc.dart';
import 'package:pressy_client/blocs/order/order_event.dart';
import 'package:pressy_client/data/data_source/coupon/coupon_data_source.dart';
import 'package:pressy_client/data/data_source/data_source.dart';
import 'package:pressy_client/data/data_source/order/order_data_source.dart';
import 'package:pressy_client/data/model/model.dart';
import 'package:pressy_client/services/di/service_provider.dart';
import 'package:pressy_client/utils/style/app_theme.dart';
import 'package:pressy_client/widgets/common/layouts/slot_widget.dart';
import 'package:pressy_client/widgets/order/base_step_widget.dart';
import 'package:intl/intl.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pressy_client/widgets/orders/promo_widget.dart';
import 'package:time_slot_picker/time_slot_picker.dart';

import 'dual_slot_widget.dart';

typedef void SlotSelectedCallback(Slot slot);
typedef void CommentCallback(String comment);
typedef void OnCouponAppliedCallback(
    String category, double couponValue, Coupon coupon);

class SlotWidget extends StatefulWidget {
  final String title;
  final bool isLoading;
  final List<Slot> slots;
  final SlotSelectedCallback onSlotSelected;
  final OnCouponAppliedCallback onCouponAppliedCallback;
  final VoidCallback onSlotConfirmed;
  final bool canMoveForward;
  final bool displaySlotTypeInfo;
  final SlotType slotType;
  final double promoAmount;
  final CommentCallback commentCallback;
  SlotWidget(
      {Key key,
      @required this.title,
      @required this.onSlotSelected,
      this.onCouponAppliedCallback,
      this.commentCallback,
      @required this.onSlotConfirmed,
      this.canMoveForward = false,
      this.slots = const [],
      this.isLoading = true,
      this.displaySlotTypeInfo = true,
      this.slotType,
      this.promoAmount})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _SlotWidgetState();
}

class _SlotWidgetState extends State<SlotWidget> {
  bool isBottomSheetOpen = false;
  DateFormat _dateFormat = DateFormat("EEEE dd MMM 'à' HH:mm", "fr");

//  DateFormat _dateFormat = DateFormat("EEEE dd MMM à HH:mm", "fr");
  DateFormat _timeFormat = DateFormat("HH:mm", "fr");
  DateTime _selectedTimeSlot;

//  DateTime _defaultSlot = DateTime.now();
  DateTime _selectedPickupTimeSlot;
  DateTime _selectedDeliveryTimeSlot;
  DateTime _defaultSlot;

  int _selectedTab = 0;
  PersistentBottomSheetController _controller;
  double _promoAmount = 0;

  TextEditingController commentController;

  List<Slot> get _standardSlots {
    var slots = this
        .widget
        .slots
        .where((slot) => slot.slotType == SlotType.STANDARD)
        .toList();
    slots.sort((lhs, rhs) => lhs.startDate.compareTo(rhs.startDate));
    return slots;
  }

  List<Slot> get _expressSlots {
    var slots = this
        .widget
        .slots
        .where((slot) => slot.slotType == SlotType.EXPRESS)
        .toList();
    slots.sort((lhs, rhs) => lhs.startDate.compareTo(rhs.startDate));
    return slots;
  }

  Slot _selectedSlot;

  void _setSelectedSlot(Slot slot) {
    this.widget.onSlotSelected(slot);
  }

  @override
  void initState() {
    super.initState();
    if (this.widget.slotType != null) {
      this._selectedTab = this.widget.slotType == SlotType.STANDARD ? 0 : 1;
    }
    if (this.widget.promoAmount != null) {
      this._promoAmount = this._promoAmount;
    }
    commentController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    if (!this.widget.isLoading &&
        this._selectedSlot == null &&
        this.widget.slots.isNotEmpty) {
      this._setSelectedSlot(this.widget.slots[0]);
    }
    return BaseStepWidget(
      title: this.widget.title,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: this._buildWidgets().toList(),
      ),
    );
  }

  Iterable<Widget> _buildWidgets() sync* {
    if (this.widget.displaySlotTypeInfo) {
      yield CupertinoSegmentedControl<int>(
          borderColor: ColorPalette.orange,
          selectedColor: ColorPalette.orange,
          unselectedColor: Colors.white,
          groupValue: this._selectedTab,
          children: {0: Text("Standard"), 1: Text("Express")},
          onValueChanged: (index) {
            this.setState(() => this._selectedTab = index);
            if (this.isBottomSheetOpen) {
              _controller?.close();
            }
          });
      yield SizedBox(height: 24);
    }
    yield this._slotInformationWidget(
        this._selectedTab == 0 ? SlotType.STANDARD : SlotType.EXPRESS);
  }

  Iterable<Widget> _buildSlotInfoWidgets(SlotType slotType) sync* {
    var orderState = BlocProvider.of<OrderBloc>(this.context).currentState;

    if (this.widget.displaySlotTypeInfo) {
      yield Row(
        children: <Widget>[
          Text("• Créneau de : ",
              style: TextStyle(color: ColorPalette.textGray)),
          Text("30 minutes", style: TextStyle(fontWeight: FontWeight.w600)),
        ],
      );
      yield SizedBox(height: 8);
      yield Row(
        children: <Widget>[
          Text("• Frais de service : ",
              style: TextStyle(color: ColorPalette.textGray)),
          Text(slotType == SlotType.STANDARD ? "GRATUIT" : "8.99€",
              style: TextStyle(fontWeight: FontWeight.w600)),
        ],
      );
      yield SizedBox(height: 8);
      yield Row(
        children: <Widget>[
          Text("• Délai de livraison : ",
              style: TextStyle(color: ColorPalette.textGray)),
          Text(slotType == SlotType.STANDARD ? "48h" : "24h",
              style: TextStyle(fontWeight: FontWeight.w600)),
        ],
      );
      yield SizedBox(height: 8);
      yield SizedBox(
        height: 48,
        child: CupertinoTextField(
          controller: commentController,
          onChanged: this.widget.commentCallback,
          onSubmitted: this.widget.commentCallback,
          prefix: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(
              Icons.edit,
              color: ColorPalette.textGray,
            ),
          ),
          placeholder: " Avez-vous une requête particulier?",
          decoration: BoxDecoration(
            border: Border.all(
              color: ColorPalette.borderGray,
            ),
            borderRadius: BorderRadius.circular(5.0),
          ),
        ),
      );
      yield SizedBox(height: 7);
      yield orderState.isCouponValid
          ? Container(
              child: Row(
                children: <Widget>[
                  Flexible(
                    child: Text(
                      "Félicitations!, Vous avez ${orderState.category == "amount" ? orderState.coupon.amountOff : orderState.coupon.percentOff} € de réduction sur votre commande.",
                      style: TextStyle(
                          fontStyle: FontStyle.italic,
                          color: ColorPalette.orange),
                    ),
                  ),
                  FlatButton(
                      onPressed: () {
                        setState(() {
                          this._promoAmount = 0;
                        });
                        this.widget.onCouponAppliedCallback("", 0, null);
                      },
                      shape: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red),
                          borderRadius: BorderRadius.circular(8)),
                      child: Text(
                        "Annuler",
                        style: TextStyle(color: Colors.red),
                      ))
                ],
              ),
            )
          : ListTile(
              leading: Icon(
                FontAwesomeIcons.gift,
                color: ColorPalette.orange,
              ),
              title: Text(
                "Entrez votre code promotionnel ou code de parrainage",
                style: TextStyle(color: ColorPalette.orange, fontSize: 13.0),
              ),
              onTap: () {
                //TODO:Design new Screen for promo-code
                final services = ServiceProvider.of(this.context);
                Future<Coupon> future = Navigator.push(
                    this.context,
                    MaterialPageRoute(
                        builder: (_) => ServiceProvider(
                            child: PromoScreen(), services: services)));

                future.then((Coupon coupon) {
                  if (coupon != null) {
                    if (coupon.amountOff == null) {
                      setState(() {
                        this._promoAmount = coupon.percentOff;
                      });
                      this.widget.onCouponAppliedCallback(
                          "percent", this._promoAmount, coupon);
                    } else {
                      setState(() {
                        this._promoAmount = coupon.amountOff.toDouble();
                      });
                      this.widget.onCouponAppliedCallback(
                          "amount", this._promoAmount, coupon);
                    }
                  }
                });
              },
            );
    }
    yield SizedBox(height: MediaQuery.of(context).size.height * 0.03);
    yield Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Récupération",
          style: TextStyle(
              color: ColorPalette.orange, fontWeight: FontWeight.bold),
        ),
        Expanded(
          child: Container(),
        ),
      ],
    );
    yield Divider();
    if (!this.widget.isLoading) this._defaultSlot = getDefaultSlot();
    yield ListTile(
      title: Text(
        getSlotWidgetText(),
//      title: Text(
//        _selectedTimeSlot == null
//            ? "${_dateFormat.format(_defaultSlot)}"
//            : "${_dateFormat.format(_selectedTimeSlot)} - ${_timeFormat.format(_selectedTimeSlot.add(Duration(minutes: 30)))}",
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
      ),
      trailing: Icon(
        FontAwesomeIcons.solidArrowAltCircleDown,
        color: ColorPalette.orange,
      ),
      onTap: () {
        createBottomSheet();
      },
    );
    yield Divider();
    yield SizedBox(height: MediaQuery.of(context).size.height * 0.03);
    yield this._nextButton(this.widget.canMoveForward);
  }

  Widget _slotInformationWidget(SlotType slotType) {
    getDefaultSlot();
    return Column(children: this._buildSlotInfoWidgets(slotType).toList());
  }

  Widget get _loadingWidget => Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircularProgressIndicator(),
              SizedBox(height: 8),
              Text("Chargement des créneaux")
            ],
          ),
        ),
      );

  void createBottomSheet() {
    setState(() {
      this.isBottomSheetOpen = true;
    });
    _controller = Scaffold.of(context).showBottomSheet((context) {
      return BottomSheet(
        onClosing: () {
          this._controller = null;
        },
        builder: (BuildContext context) {
          return SlotUI(
            slots: this._selectedTab == 0
                ? this._standardSlots
                : this._expressSlots,
            onSlotSelected: (slot) {
              this._setSelectedSlot(slot);
//              setState(() {
//                _selectedTimeSlot = slot.startDate;
//              });
              if (this.widget.displaySlotTypeInfo) {
                setState(() {
                  _selectedPickupTimeSlot = slot.startDate;
                  _selectedSlot = slot;
                });
              } else {
                setState(() {
                  _selectedDeliveryTimeSlot = slot.startDate;
                  _selectedSlot = slot;
                });
              }
            },
          );
        },
        elevation: 60,
      );
    });
    this._controller.closed.then((val) {
      _controller = null;
      setState(() {
        this.isBottomSheetOpen = false;
      });
    });
  }

  Widget get _slotListWidget => this.widget.isLoading
      ? this._loadingWidget
      : GestureDetector(
          child: Container(
            child: DualSlotWidget(
              useMagnifier: true,
              backgroundColor: Colors.transparent,
              itemExtent: Theme.of(context).textTheme.display2.fontSize,
              onSelectedItemChanged: (index) {
                this._setSelectedSlot(this._selectedTab == 0
                    ? this._standardSlots[index]
                    : this._expressSlots[index]);
              },
              slots: this._selectedTab == 0
                  ? this._standardSlots
                  : this._expressSlots,
            ),
          ),
          onTap: () {},
        );

  Widget _nextButton(bool enabled) => Row(
        children: <Widget>[
          Expanded(
            child: Container(
              height: 40,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color:
                      enabled ? ColorPalette.orange : ColorPalette.lightGray),
              child: ButtonTheme(
                height: double.infinity,
                child: FlatButton(
                    child: Text("SUIVANT"),
                    textColor: Colors.white,
                    onPressed: enabled
                        ? () {
                            if (_selectedDeliveryTimeSlot == null)
                              this._setSelectedSlot(
                                  getDefaultSlot(dateTime: false));

                            this.widget.onSlotConfirmed();
                          }
                        : null),
              ),
            ),
          )
        ],
      );

  String getSlotWidgetText() {
    if (this.widget.displaySlotTypeInfo) {
      if (_selectedPickupTimeSlot == null) {
        if (_defaultSlot != null) {
          return "${_dateFormat.format(_defaultSlot)} - ${_timeFormat.format(_defaultSlot.add(Duration(minutes: 30)))}";
        } else {
          return "Aucun emplacement à afficher.";
        }
      } else {
        return "${_dateFormat.format(_selectedPickupTimeSlot)} - ${_timeFormat.format(_selectedPickupTimeSlot.add(Duration(minutes: 30)))}";
      }
    } else {
      if (_selectedDeliveryTimeSlot == null) {
        if (_defaultSlot != null) {
          return "${_dateFormat.format(_defaultSlot)} - ${_timeFormat.format(_defaultSlot.add(Duration(minutes: 30)))}";
        } else {
          return "Aucun emplacement à afficher.";
        }
      } else {
        return "${_dateFormat.format(_selectedDeliveryTimeSlot)} - ${_timeFormat.format(_selectedDeliveryTimeSlot.add(Duration(minutes: 30)))}";
      }
    }
  }

  getDefaultSlot({bool dateTime = true}) {
    // List<Slot> _standardSlots =[];
    var _standardSlots = this
        .widget
        .slots
        .where((slot) => slot.slotType == SlotType.STANDARD)
        .toList();
    _standardSlots.sort((lhs, rhs) => lhs.startDate.compareTo(rhs.startDate));

    var _expressSlots = this
        .widget
        .slots
        .where((slot) => slot.slotType == SlotType.EXPRESS)
        .toList();
    _expressSlots.sort((lhs, rhs) => lhs.startDate.compareTo(rhs.startDate));

    if (dateTime) {
      if (this.widget.displaySlotTypeInfo) {
        if (this._selectedTab == 0 && _standardSlots.isNotEmpty) {
          return this._standardSlots[0].startDate;
        }
        if (this._selectedTab == 1 && _expressSlots.isNotEmpty) {
          return this._expressSlots[0].startDate;
        }
      } else {
        if (this._selectedTab == 0 && _standardSlots.isNotEmpty) {
          return this._standardSlots[0].startDate;
        }
        if (this._selectedTab == 1 && _expressSlots.isNotEmpty) {
          return this._expressSlots[0].startDate;
        }
      }
    } else {
      if (this.widget.displaySlotTypeInfo) {
        if (this._selectedTab == 0 && _standardSlots.isNotEmpty) {
          return this._standardSlots[0];
        }
        if (this._selectedTab == 1 && _expressSlots.isNotEmpty) {
          return this._expressSlots[0];
        }
      } else {
        if (this._selectedTab == 0 && _standardSlots.isNotEmpty) {
          return this._standardSlots[0];
        }
        if (this._selectedTab == 1 && _expressSlots.isNotEmpty) {
          return this._expressSlots[0];
        }
      }
    }
  }
}
