import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:pressy_client/data/model/order/slot/slot.dart';

typedef void OnSelectedItemChanged(int index);

class DualSlotWidget extends StatefulWidget {
  final List<Slot> slots;
  final Color backgroundColor;
  final bool useMagnifier;
  final double itemExtent;
  final double magnification;
  final OnSelectedItemChanged onSelectedItemChanged;
  const DualSlotWidget(
      {Key key,
      @required this.slots,
      this.backgroundColor,
      this.itemExtent,
      this.magnification,
      this.useMagnifier,
      this.onSelectedItemChanged})
      : super(key: key);
  @override
  _DualSlotWidgetState createState() => _DualSlotWidgetState();
}

class _DualSlotWidgetState extends State<DualSlotWidget> {
//  DateFormat _dateFormat = DateFormat("EEEEddMMM HH'h'mm", "fr");
  DateFormat _dateFormat = DateFormat("EEEE dd MMM", "fr");
  DateFormat _timeFormat = DateFormat("HH'h'mm", "fr");

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 600,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: 100,
            height: Theme.of(context).textTheme.display2.fontSize + 60,
            child: CupertinoPicker(
              backgroundColor: widget.backgroundColor ?? Colors.transparent,
              onSelectedItemChanged: this.widget.onSelectedItemChanged,
              useMagnifier: this.widget.useMagnifier ?? true,
              itemExtent: this.widget.itemExtent + 10 ??
                  Theme.of(context).textTheme.display2.fontSize + 5,
              magnification: this.widget.magnification ?? 1.2,
              children: this
                  .widget
                  .slots
                  .map((slot) => Container(
                        padding: EdgeInsets.all(8),
                        child: Center(
                          child: Text(
                            "${this._dateFormat.format(slot.startDate)}",
                            style:
                                TextStyle(fontFamily: 'Avenir', fontSize: 16),
                          ),
                        ),
                      ))
                  .toList(),
            ),
          ),
          SizedBox(
            width: 200,
            height: Theme.of(context).textTheme.display2.fontSize + 60,
            child: CupertinoPicker(
              backgroundColor:
                  this.widget.backgroundColor ?? Colors.transparent,
              onSelectedItemChanged: this.widget.onSelectedItemChanged,
              useMagnifier: this.widget.useMagnifier ?? true,
              itemExtent: this.widget.itemExtent ??
                  Theme.of(context).textTheme.display2.fontSize + 5,
              magnification: this.widget.magnification ?? 1.2,
              children: this
                  .widget
                  .slots
                  .map((slot) => Container(
                        padding: EdgeInsets.all(8),
                        child: Center(
                          child: Text(
//                            "${this._timeFormat.format(slot.startDate)}",
                            "${this._timeFormat.format(slot.startDate)}  -  ${this._timeFormat.format(slot.startDate.add(Duration(minutes: 30)))}",
                            style: TextStyle(fontWeight: FontWeight.w400),
                          ),
                        ),
                      ))
                  .toList(),
            ),
          ),
        ],
      ),
    );
  }
}
