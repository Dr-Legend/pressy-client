//import 'package:flutter/material.dart';
//import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:pressy_client/blocs/numeric_stepper/numeric_stepper_bloc.dart';
//import 'package:pressy_client/blocs/order/order_bloc.dart';
//import 'package:pressy_client/blocs/order/order_event.dart';
//import 'package:pressy_client/data/model/model.dart';
//import 'package:pressy_client/utils/style/app_theme.dart';
//
//typedef void OnCounterChanged(int itemCount);
//
//class NumericStepper extends StatefulWidget {
//  final OnCounterChanged onValueChanged;
//  final OrderBloc orderBloc;
//  final Map<Article, int> cart;
//  final Article article;
//  const NumericStepper(
//      {Key key, this.cart, this.article, this.onValueChanged, this.orderBloc})
//      : super(key: key);
//
//  @override
//  State<StatefulWidget> createState() => _NumericStepperState();
//}
//
//class _NumericStepperState extends State<NumericStepper> {
//  @override
//  Widget build(BuildContext context) {
//    return BlocProvider.value(
//      value: NumericStepperBloc(),
//      child: BlocBuilder<NumericStepperBloc, NumericStepperState>(
//          builder: (BuildContext context, NumericStepperState state) {
//        var numericBloc = BlocProvider.of<NumericStepperBloc>(context);
//        return Container(
//          decoration: BoxDecoration(
//              borderRadius: BorderRadius.circular(4),
//              border: Border.all(color: ColorPalette.orange, width: 1)),
//          child: Row(
//            mainAxisSize: MainAxisSize.min,
//            children: <Widget>[
//              Container(
//                width: 32,
//                height: 32,
//                color: ColorPalette.orange,
//                child: Center(
//                    child: ButtonTheme(
//                        height: 18,
//                        child: IconButton(
//                            icon: Icon(Icons.remove,
//                                size: 18, color: Colors.white),
//                            onPressed: state.itemCount > 0
//                                ? () {
//                                    numericBloc.dispatch(DecrementCounter());
////                                    this.widget.onValueChanged(state.itemCount);
//                                  }
//                                : null))),
//              ),
//              Container(
//                width: 32,
//                height: 32,
//                color: Colors.transparent,
//                child: Center(child: Text("${state.itemCount}")),
//              ),
//              Container(
//                width: 32,
//                height: 32,
//                color: ColorPalette.orange,
//                child: Center(
//                    child: ButtonTheme(
//                  height: 18,
//                  child: IconButton(
//                      icon: Icon(Icons.add, size: 18, color: Colors.white),
//                      onPressed: () {
//                        numericBloc.dispatch(IncrementCounter());
//                        this.widget.orderBloc.dispatch(CartChangedEvent(
//                            this.widget.article, state.itemCount));
//
//                        this
//                            .widget
//                            .orderBloc
//                            .dispatch(TotalChangedEvent(this.widget.cart));
//                        this.widget.orderBloc.dispatch(
//                            EstimatePriceChangedEvent(this.widget.cart));
////                        this.widget.onValueChanged(state.itemCount);
//                      }),
//                )),
//              )
//            ],
//          ),
//        );
//      }),
//    );
//  }
//
////  void _add() => this.setState(() {
////        this._value++;
////        this.widget.onValueChanged(this._value);
////      });
//
////  void _subtract() => this.setState(() {
////        this._value--;
////        this.widget.onValueChanged(this._value);
////      });
//}
