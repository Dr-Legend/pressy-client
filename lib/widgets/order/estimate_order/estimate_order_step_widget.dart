//import 'dart:math';
//
//import 'package:flutter/material.dart';
//import 'package:flutter/cupertino.dart';
//import 'package:pressy_client/blocs/order/order_bloc.dart';
//import 'package:pressy_client/blocs/order/order_event.dart';
//import 'package:pressy_client/data/model/model.dart';
//import 'package:pressy_client/utils/style/app_theme.dart';
//import 'package:pressy_client/widgets/order/base_step_widget.dart';
//import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
//import 'package:pressy_client/widgets/order/stepper/numeric_stepper.dart';
//
//typedef void OnEstimateOrderFinished(
//    OrderType orderType, double estimatedPrice);
//typedef void OnOrderPriceChanged(
//    double estimatedPrice, OrderType orderType, totalPrice);
//
//class EstimateOrderStepWidget extends StatefulWidget {
//  final Article weightedArticle;
//  final List<Article> articles;
//  final OnEstimateOrderFinished onFinish;
//  final OnOrderPriceChanged onChange;
//  final OrderBloc orderBloc;
//  EstimateOrderStepWidget(
//      {Key key,
//      this.articles,
//      this.onFinish,
//      @required this.orderBloc,
//      @required this.onChange,
//      @required this.weightedArticle})
//      : super(key: key);
//
//  @override
//  State<StatefulWidget> createState() => _EstimateOrderStepWidgetState();
//}
//
//class _EstimateOrderStepWidgetState extends State<EstimateOrderStepWidget> {
//  int _selectedIndex = 0;
//  Map<Article, int> _cart = {};
//  double _totalPrice = 0.0;
//  List<Article> arrangedArticles = [];
//
//  @override
//  void initState() {
//    super.initState();
//  }
//
//  @override
//  void didChangeDependencies() {
//
//    super.didChangeDependencies();
//  }
//
//  @override
//  void didUpdateWidget(EstimateOrderStepWidget oldWidget) {
////    rearrangeArticles();
//    super.didUpdateWidget(oldWidget);
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return BaseStepWidget(
//      title: "Estimer votre commande",
//      child: Column(
//        crossAxisAlignment: CrossAxisAlignment.stretch,
//        children: <Widget>[
//          CupertinoSegmentedControl<int>(
//              borderColor: ColorPalette.orange,
//              selectedColor: ColorPalette.orange,
//              unselectedColor: Colors.white,
//              groupValue: this._selectedIndex,
//              children: {0: Text("Pressing"), 1: Text("Linge au kilo")},
//              onValueChanged: (index) {
//                this.setState(() => this._selectedIndex = index);
//              }),
//          this._selectedIndex == 0
//              ? this._laundryWidget
//              : this._weightedServiceWidget
//        ],
//      ),
//    );
//  }
//
//  rearrangeArticles() {
//    //PROMOTION – Lot 10 chemises (au lieu de 39 euros)
//    //PROMOTION – Lot 15 chemises (au lieu de 58,50 euros)
//    //PROMOTION – Lot 3 costumes (au lieu de 47,50 euros)
////    if (this.widget.articles.length != 0) {
//    this.arrangedArticles = [];
//    Article tShirt = this
//        .widget
//        .articles
//        .firstWhere((article) => article.name == "T-shirt", orElse: () => null);
////    print("T-shirt Found!=======> ${tShirt.name} ${tShirt.laundryPrice} ");
//    Article robe = this
//        .widget
//        .articles
//        .firstWhere((robe) => robe.name == "Robe", orElse: () => null);
//    Article costume3Pieces = this.widget.articles.firstWhere(
//        (costume3p) => costume3p.name == "Costume 3 pièces",
//        orElse: () => null);
//    Article costume2Pieces = this.widget.articles.firstWhere(
//        (costume2p) => costume2p.name == "Costume 2 pièces",
//        orElse: () => null);
//
//    Article chemisePliee = this.widget.articles.firstWhere(
//        (chemisePliee) => chemisePliee.name == "Chemise pliée",
//        orElse: () => null);
//
//    Article topChemier = this.widget.articles.firstWhere(
//        (topChemesier) =>
//            topChemesier.name == "Top ou Chemisier femme -repassage à la main",
//        orElse: () => null);
//    Article promotion1 = this.widget.articles.firstWhere(
//        (promotion1) =>
//            promotion1.name ==
//            "PROMOTION – Lot 10 chemises (au lieu de 39 euros)",
//        orElse: () => null);
//    Article promotion2 = this.widget.articles.firstWhere(
//        (promotion2) =>
//            promotion2.name ==
//            "PROMOTION – Lot 3 costumes (au lieu de 47,50 euros)",
//        orElse: () => null);
//    Article promotion3 = this.widget.articles.firstWhere(
//        (promotion3) =>
//            promotion3.name ==
//            "PROMOTION – Lot 15 chemises (au lieu de 58,50 euros)",
//        orElse: () => null);
//
//    if (promotion1 != null) this.arrangedArticles.add(promotion1);
//    if (promotion2 != null) this.arrangedArticles.add(promotion2);
//    if (promotion3 != null) this.arrangedArticles.add(promotion3);
//    if (costume2Pieces != null) this.arrangedArticles.add(costume2Pieces);
//
//    if (chemisePliee != null) {
//      chemisePliee.name
//          .replaceAll("Chemise pliée", "Chemise pliée -repassage à la main");
//      this.arrangedArticles.add(chemisePliee);
//    }
//
//    if (topChemier != null) this.arrangedArticles.add(topChemier);
//    if (robe != null) this.arrangedArticles.add(robe);
//    if (tShirt != null) this.arrangedArticles.add(tShirt);
//    print("Articles filled length is :${this.arrangedArticles.length}");
//    this.widget.articles.forEach((article) {
//      if (article != tShirt &&
//          article != robe &&
//          article != costume2Pieces &&
//          article != costume3Pieces &&
//          article != topChemier &&
//          article != promotion3 &&
//          article != promotion2 &&
//          article != chemisePliee &&
//          article != promotion1) {
//        this.arrangedArticles.add(article);
//      }
//    });
////    } else {
////      print("ARTICLES ARE EMPTY!");
////    }
////    print(
////        "T-shirt added at index 10 ${this.arrangedArticles.indexOf(tShirt)} Lenght is ${this.arrangedArticles.length}");
//  }
//
//  Widget get _weightedServiceWidget => Column(
//        children: <Widget>[
//          SizedBox(height: 18),
//          Text(
//            "Prix/kg",
//            style: TextStyle(color: ColorPalette.darkGray),
//          ),
//          SizedBox(height: 18),
//          StaggeredGridView.countBuilder(
//            shrinkWrap: true,
//            itemCount: 1,
//            crossAxisCount: 1,
//            itemBuilder: (context, index) =>
//                this._buildArticleWidget(this.widget.weightedArticle),
//            staggeredTileBuilder: (index) => StaggeredTile.fit(1),
//            crossAxisSpacing: 12,
//            mainAxisSpacing: 12,
//            physics: NeverScrollableScrollPhysics(),
//          ),
//          SizedBox(height: 12),
//          //this._priceAndPassButtonWidget
//        ],
//      );
//
//  Widget get _laundryWidget => Column(
//        children: <Widget>[
//          SizedBox(height: 18),
//          Text(
//            "Veuillez sélectionner vos articles afin d'établir un devis/\n celui-ci sera confirmé lors de la collecte.",
//            style: TextStyle(color: ColorPalette.darkGray),
//          ),
//          SizedBox(height: 18),
//          GridView.count(
//            shrinkWrap: true,
//            physics: NeverScrollableScrollPhysics(),
//            crossAxisCount: 2,
//            children: this
//                .widget
//                .articles
//                .map((article) => this._buildArticleWidget(article))
//                .toList(),
//            childAspectRatio: 0.4,
//            mainAxisSpacing: 12,
//            crossAxisSpacing: 12,
//          ),
//          SizedBox(height: 12),
//          //this._priceAndPassButtonWidget
//        ],
//      );
//
//  Widget get _priceAndPassButtonWidget => Container(
//        padding: EdgeInsets.only(top: 12),
//        decoration: BoxDecoration(
//            border: Border(
//                top: BorderSide(color: ColorPalette.borderGray, width: 1))),
//        child: Row(
//          mainAxisSize: MainAxisSize.max,
//          children: <Widget>[
//            Text("Total :",
//                style: TextStyle(color: ColorPalette.darkGray, fontSize: 14)),
//            Text("${this._totalPrice.toStringAsFixed(2)} €",
//                style: TextStyle(
//                    color: ColorPalette.textBlack,
//                    fontWeight: FontWeight.bold)),
//            Expanded(child: Container()),
//            FlatButton(
//                onPressed: () => this.widget.onFinish(
//                    this._selectedIndex == 0
//                        ? OrderType.PRESSING
//                        : OrderType.WEIGHT,
//                    this._calculateTotalPrice()),
//                child: Text("SUIVANT",
//                    style: TextStyle(color: ColorPalette.orange))),
//          ],
//        ),
//      );
//
//  Widget _buildArticleWidget(Article article) => Container(
//        key: UniqueKey(),
//        padding: EdgeInsets.all(12),
//        decoration: BoxDecoration(
//            border: Border.all(color: ColorPalette.borderGray, width: 1),
//            borderRadius: BorderRadius.circular(4),
//            color: Colors.white),
//        child: Column(
//          mainAxisSize: MainAxisSize.min,
//          children: <Widget>[
//            SizedBox(height: 200, child: Image.network(article.photoUrl)),
//            SizedBox(
//              height: 50,
//            ),
//            Text(
//              article.name,
//              style: TextStyle(fontWeight: FontWeight.bold),
//              maxLines: 2,
//              overflow: TextOverflow.ellipsis,
//            ),
//            SizedBox(height: 4),
//            Text("${article.laundryPrice}€",
//                style: TextStyle(
//                    color: ColorPalette.darkGray, fontWeight: FontWeight.bold)),
//            SizedBox(height: 12),
//            NumericStepper(
//              article: article,
//              cart: this.widget.orderBloc.currentState.cart,
//              orderBloc: this.widget.orderBloc,
//              onValueChanged: (value) {
////                this._cart[article] = value;
////
////                this.widget.orderBloc.dispatch(OrderTypeChangedEvent(
////                      this._selectedIndex == 0
////                          ? OrderType.PRESSING
////                          : OrderType.WEIGHT,
////                    ));
////                this.widget.onChange(
////                    this._totalPrice,
////                    this._selectedIndex == 0
////                        ? OrderType.PRESSING
////                        : OrderType.WEIGHT,
////                    this._calculateTotalPrice());
////                this.setState(() {
////                  this._totalPrice = this._calculateTotalPrice();
////                  this.widget.onChange(
////                      this._totalPrice,
////                      this._selectedIndex == 0
////                          ? OrderType.PRESSING
////                          : OrderType.WEIGHT,
////                      this._calculateTotalPrice());
////                });
//              },
//            )
//          ],
//        ),
//      );
//
//  double _calculateTotalPrice() {
//    double priceAccumulator = 0.0;
//    this._cart.forEach((article, count) {
//      priceAccumulator += article.laundryPrice * count;
//    });
//    return priceAccumulator;
//  }
//}
