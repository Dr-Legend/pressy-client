import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:pressy_client/blocs/auth/auth_bloc.dart';
import 'package:pressy_client/blocs/auth/sign_up/sign_up_bloc.dart';
import 'package:pressy_client/blocs/auth/sign_up/sign_up_event.dart';
import 'package:pressy_client/blocs/auth/sign_up/sign_up_state.dart';
import 'package:pressy_client/data/data_source/member/member_data_source.dart';
import 'package:pressy_client/data/session/member/member_session.dart';
import 'package:pressy_client/services/di/service_provider.dart';
import 'package:pressy_client/utils/style/app_theme.dart';
import 'package:pressy_client/utils/validators/validators.dart';
import 'package:pressy_client/widgets/common/mixins/error_mixin.dart';
import 'package:pressy_client/widgets/common/mixins/lifecycle_mixin.dart';
import 'package:pressy_client/widgets/common/mixins/loader_mixin.dart';
import 'package:pressy_client/widgets/common/widgets/background.dart';
import 'package:pressy_client/widgets/common/widgets/link_text_span.dart';

class SignUpWidget extends StatefulWidget {
  final AuthBloc authBloc;
  final IMemberSession memberSession;
  final VoidCallback onAuthCompleted;
  final IMemberDataSource memberDataSource;
  final VoidCallback onBackPressed;
  const SignUpWidget(
      {Key key,
      this.authBloc,
      this.memberSession,
      this.onAuthCompleted,
      this.memberDataSource,
      @required this.onBackPressed})
      : super(key: key);

  @override
  _SignUpWidgetState createState() => _SignUpWidgetState();
}

class _SignUpWidgetState extends State<SignUpWidget>
    with WidgetLifeCycleMixin, LoaderMixin, ErrorMixin {
  PDFDocument assetFilePath;
  SignUpBloc _signUpBloc;
  TextEditingController _firstNameFieldController = TextEditingController();
  TextEditingController _lastNameFieldController = TextEditingController();
  TextEditingController _emailFieldController = TextEditingController();
  TextEditingController _phoneNumberFieldController = TextEditingController();
  TextEditingController _passwordFieldController = TextEditingController();
  TextEditingController _passwordConfirmationFieldController =
      TextEditingController();

  bool _acceptsTerms = false;

  @override
  void initState() {
    this._signUpBloc = SignUpBloc(
        authBloc: this.widget.authBloc,
        memberDataSource: widget.memberDataSource,
        memberSession: this.widget.memberSession);
    getPDFDocumentFromAsset(asset: "assets/props/terms-conditions.pdf")
        .then((pdfFile) {
      setState(() {
        assetFilePath = pdfFile;
      });
    });
    getPDFDocumentFromAsset(asset: "assets/props/terms-conditions.pdf")
        .then((pdfFile) {
      setState(() {
        assetFilePath = pdfFile;
      });
    });
    super.initState();
  }

  Future<PDFDocument> getPDFDocumentFromAsset({@required String asset}) async {
    PDFDocument doc = await PDFDocument.fromAsset(asset);

    return doc;
  }

  @override
  void dispose() {
    _signUpBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: SafeArea(
          child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.white10,
          leading: IconButton(
              icon: Icon(
                CupertinoIcons.left_chevron,
                color: ColorPalette.textBlack,
              ),
              onPressed: () => this.widget.onBackPressed()),
        ),
        body: Builder(
          builder: (BuildContext context) => BlocProvider.value(
            value: this._signUpBloc,
            child: BlocBuilder(
                bloc: this._signUpBloc,
                builder: (BuildContext context, SignUpState state) {
                  this.onWidgetDidBuild(
                      () => this._handleState(state, context));
                  return Stack(
                    children: <Widget>[
                      Background(),
                      SingleChildScrollView(
                        child: Form(
                          autovalidate: true,
                          onChanged: this._signUpFormChanged,
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(
                                    top:
                                        MediaQuery.of(context).size.height / 4),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 8.0),
                                child: Container(
                                  child: Text(
                                    "créer un compte",
                                    style: TextStyle(
                                        fontSize: 30,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 13,
                              ),
                              buildFirstNameInput(),
                              buildLastNameInput(),
                              buildEmailInput(),
                              buildPhoneNumberInput(),
                              buildPasswordInput(),
                              buildPasswordConfirmationInput(),
                              buildAcceptTermsInput(),
                              roundedRectButton(
                                  title: "se connecter",
                                  gradient: state is SignUpInitialState &&
                                          state.isValid
                                      ? signUpGradients
                                      : signUpDisableGradients,
                                  isEndIconVisible:
                                      state is SignUpInitialState &&
                                          state.isValid),
                            ],
                          ),
                        ),
                      )
                    ],
                  );
                }),
          ),
        ),
      )),
    );
  }

  Widget roundedRectButton(
      {String title, List<Color> gradient, bool isEndIconVisible}) {
    return InkWell(
      onTap: isEndIconVisible
          ? () => this._signUpBloc.dispatch(SignUpButtonPressedEvent(
              email: this._emailFieldController.text,
              password: this._passwordFieldController.text,
              firstName: this._firstNameFieldController.text,
              lastName: this._lastNameFieldController.text,
              phoneNumber: this._phoneNumberFieldController.text,
              passwordConfirmation:
                  this._passwordConfirmationFieldController.text))
          : null,
      child: Padding(
        padding: EdgeInsets.only(bottom: 10),
        child: Stack(
          alignment: Alignment(1.0, 0.0),
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width / 1.7,
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0)),
                gradient: LinearGradient(
                    colors: gradient,
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight),
              ),
              child: Text(title,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w500)),
              padding: EdgeInsets.only(top: 16, bottom: 16),
            ),
            Visibility(
              visible: isEndIconVisible,
              child: Padding(
                  padding: EdgeInsets.only(right: 10),
                  child: ImageIcon(
                    AssetImage("assets/ic_forward.png"),
                    size: 30,
                    color: Colors.white,
                  )),
            ),
          ],
        ),
      ),
    );
  }

  List<Color> signUpGradients = [
    Color(0xFFFF9945),
    Color(0xFFFc6076),
  ];
  List<Color> signUpDisableGradients = [
    ColorPalette.lightGray,
    ColorPalette.lightGray,
  ];

  Widget buildEmailInput() {
    return Padding(
      padding: EdgeInsets.only(right: 40, bottom: 10),
      child: Container(
        width: MediaQuery.of(context).size.width - 40,
        child: Material(
          elevation: 10,
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(0.0),
                  topRight: Radius.circular(30))),
          child: Padding(
            padding: EdgeInsets.only(left: 40, right: 20, top: 10, bottom: 10),
            child: _emailField,
          ),
        ),
      ),
    );
  }

  Widget buildPasswordInput() {
    return Padding(
      padding: EdgeInsets.only(right: 40, bottom: 10),
      child: Container(
        width: MediaQuery.of(context).size.width - 40,
        child: Material(
          elevation: 10,
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(0.0),
                  topRight: Radius.circular(30))),
          child: Padding(
            padding: EdgeInsets.only(left: 40, right: 20, top: 10, bottom: 10),
            child: _passwordField,
          ),
        ),
      ),
    );
  }

  Widget buildFirstNameInput() {
    return Padding(
      padding: EdgeInsets.only(right: 40, bottom: 10),
      child: Container(
        width: MediaQuery.of(context).size.width - 40,
        child: Material(
          elevation: 10,
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(0.0),
                  topRight: Radius.circular(30))),
          child: Padding(
            padding: EdgeInsets.only(left: 40, right: 20, top: 10, bottom: 10),
            child: _firstNameField,
          ),
        ),
      ),
    );
  }

  Widget buildLastNameInput() {
    return Padding(
      padding: EdgeInsets.only(right: 40, bottom: 10),
      child: Container(
        width: MediaQuery.of(context).size.width - 40,
        child: Material(
          elevation: 10,
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(0.0),
                  topRight: Radius.circular(30))),
          child: Padding(
            padding: EdgeInsets.only(left: 40, right: 20, top: 10, bottom: 10),
            child: _lastNameField,
          ),
        ),
      ),
    );
  }

  Widget buildPhoneNumberInput() {
    return Padding(
      padding: EdgeInsets.only(right: 40, bottom: 30),
      child: Container(
        width: MediaQuery.of(context).size.width - 40,
        child: Material(
          elevation: 10,
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(0.0),
                  topRight: Radius.circular(30))),
          child: Padding(
            padding: EdgeInsets.only(left: 40, right: 20, top: 10, bottom: 10),
            child: _phoneField,
          ),
        ),
      ),
    );
  }

  Widget buildPasswordConfirmationInput() {
    return Padding(
      padding: EdgeInsets.only(right: 40, bottom: 10),
      child: Container(
        width: MediaQuery.of(context).size.width - 40,
        child: Material(
          elevation: 10,
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(0.0),
                  topRight: Radius.circular(30))),
          child: Padding(
            padding: EdgeInsets.only(left: 40, right: 20, top: 10, bottom: 10),
            child: _passwordConfirmationField,
          ),
        ),
      ),
    );
  }

  Widget buildAcceptTermsInput() {
    return Padding(
      padding: EdgeInsets.only(right: 40, bottom: 10),
      child: Container(
        width: MediaQuery.of(context).size.width - 40,
        child: Material(
          elevation: 10,
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(0.0),
                  topRight: Radius.circular(30))),
          child: Padding(
            padding: EdgeInsets.only(left: 30, right: 20, top: 10, bottom: 10),
            child: _termsCheckbox,
          ),
        ),
      ),
    );
  }

  Widget get _lastNameField => TextFormField(
        controller: this._firstNameFieldController,
        decoration: InputDecoration(labelText: "Nom", helperText: "Votre nom"),
        keyboardType: TextInputType.text,
        validator: Validators.nameValidator,
        textCapitalization: TextCapitalization.words,
      );

  Widget get _firstNameField => TextFormField(
        controller: this._lastNameFieldController,
        decoration:
            InputDecoration(labelText: "Prénom", helperText: "Votre prénom"),
        keyboardType: TextInputType.text,
        validator: Validators.nameValidator,
        textCapitalization: TextCapitalization.words,
      );

  Widget get _emailField => TextFormField(
        controller: this._emailFieldController,
        decoration: InputDecoration(
          helperText: "Votre email sera utilisé pour vous identifier",
          labelText: "Email",
        ),
        validator: Validators.emailValidator,
        keyboardType: TextInputType.emailAddress,
      );

  Widget get _phoneField => TextFormField(
        controller: this._phoneNumberFieldController,
        decoration: InputDecoration(
          helperText: "Votre téléphone sera utilisé pour vous contacter",
          labelText: "Numéro de téléphone",
        ),
        validator: Validators.phoneNumberValidator,
        keyboardType: TextInputType.phone,
      );

  Widget get _passwordField => TextFormField(
      controller: this._passwordFieldController,
      decoration: InputDecoration(
          helperText: "Votre mot de passe", labelText: "Mot de passe"),
      obscureText: true,
      validator: Validators.newPasswordValidator);

  Widget get _passwordConfirmationField => TextFormField(
      controller: this._passwordConfirmationFieldController,
      decoration: InputDecoration(
          helperText: "Confirmer votre mot de passe",
          labelText: "Confirmation"),
      obscureText: true,
      validator: (p) => Validators.passwordConfirmationValidator(
          this._passwordFieldController.text, p));

  Widget get _termsCheckbox => Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          CircularCheckBox(
            value: this._acceptsTerms,
            onChanged: (value) {
              this.setState(() => this._acceptsTerms = value);
              this._signUpFormChanged();
            },
            materialTapTargetSize: MaterialTapTargetSize.padded,
            activeColor: ColorPalette.orange,
          ),
          Flexible(
            child: RichText(
              text: TextSpan(
                children: <TextSpan>[
                  LinkTextSpan(
                      text: " conditions générales ",
                      style: TextStyle(
                        color: Colors.blue,
                      ),
                      ontap: () {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return PDFViewer(
                                document: assetFilePath,
                              );
                            });
                        print("onTap Called!");
                      }),
                  TextSpan(
                      text: "d'utilisation ",
                      style: TextStyle(color: ColorPalette.darkGray)),
                ],
                text: "J'accepte les",
                style: TextStyle(color: ColorPalette.darkGray, fontSize: 12),
              ),
            ),
          ),
        ],
      );

  void _signUpFormChanged() {
    this._signUpBloc.dispatch(SignUpSubmitFormEvent(
        email: this._emailFieldController.text,
        password: this._passwordFieldController.text,
        firstName: this._firstNameFieldController.text,
        lastName: this._lastNameFieldController.text,
        phoneNumber: this._phoneNumberFieldController.text,
        passwordConfirmation: this._passwordConfirmationFieldController.text,
        acceptsTerms: this._acceptsTerms));
  }

  void _handleState(SignUpState state, BuildContext context) {
    if (state is SignUpLoadingState)
      this.showLoaderSnackBar(context);
    else
      this.hideLoaderSnackBar(context);

    if (state is SignUpFailureState) this.showErrorDialog(context, state.error);

    if (state is SignUpSuccessState) this._openNextWidget(context);
  }

  void _openNextWidget(BuildContext context) {
    this.hideLoaderSnackBar(context);
    this.widget.onAuthCompleted();
  }
}
