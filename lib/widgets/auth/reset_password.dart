import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pressy_client/blocs/auth/auth_bloc.dart';
import 'package:pressy_client/blocs/auth/auth_event.dart';
import 'package:pressy_client/blocs/auth/auth_state.dart';
import 'package:pressy_client/data/data_source/forgot_password/forgot_password_data_source.dart';
import 'package:pressy_client/data/model/model.dart';
import 'package:pressy_client/services/di/service_provider.dart';
import 'package:pressy_client/utils/style/app_theme.dart';
import 'package:pressy_client/widgets/common/mixins/error_mixin.dart';
import 'package:pressy_client/widgets/common/mixins/lifecycle_mixin.dart';
import 'package:pressy_client/widgets/common/mixins/loader_mixin.dart';

class ResetPassword extends StatefulWidget {
  final AuthBloc authBloc;

  const ResetPassword({Key key, this.authBloc}) : super(key: key);
  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword>
    with LoaderMixin, WidgetLifeCycleMixin, ErrorMixin {
  final _formKey1 = new GlobalKey<FormState>(debugLabel: 'reset-password');

  TextEditingController verificationCodeController;

  TextEditingController newPasswordController;

  TextEditingController oldPasswordController;

  TextEditingController confirmPasswordController;

  @override
  void initState() {
    verificationCodeController = TextEditingController();
    newPasswordController = TextEditingController();
    confirmPasswordController = TextEditingController();
    oldPasswordController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            iconTheme: IconThemeData(color: ColorPalette.orange),
            backgroundColor: Colors.white,
            elevation: 0,
          ),
//          resizeToAvoidBottomInset: false,
          body: SingleChildScrollView(
            child: BlocProvider.value(
              value: this.widget.authBloc,
              child: BlocListener<AuthBloc, AuthState>(
                listener: (context, state) {
                  _handleState(state, context);
                },
                child: BlocBuilder<AuthBloc, AuthState>(
                  builder: (context, state) {
//                  this._handleState(state, context);
                    return Container(
                      color: Colors.white,
                      padding: EdgeInsets.symmetric(vertical: 20.0),
                      child: Form(
                        key: _formKey1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Container(
                                    padding: EdgeInsets.all(20),
                                    child: Icon(
                                      FontAwesomeIcons.key,
                                      color: Colors.white,
                                      size: 50,
                                    ),
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: ColorPalette.orange,
                                    )),
                                Padding(
                                  padding: const EdgeInsets.only(top: 30.0),
                                  child: Text(
                                    "réinitialiser le mot de passe",
                                    style: Theme.of(context).textTheme.title,
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 30.0, right: 30.0, top: 20),
                                    child: Text(
                                      "Entrez votre nouveau mot de passe ci-dessous, nous sommes simplement en sécurité supplémentaire..",
                                      style: Theme.of(context)
                                          .textTheme
                                          .body1
                                          .copyWith(
                                            color: ColorPalette.textGray,
                                          ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 50.0, right: 40, left: 20),
                                  child: TextFormField(
                                    controller: verificationCodeController,
                                    decoration: InputDecoration(
                                        prefixIcon: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Icon(
                                            Icons.vpn_key,
                                            color: ColorPalette.orange,
                                          ),
                                        ),
                                        disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: ColorPalette.borderGray),
                                        ),
                                        errorBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: ColorPalette.red),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: ColorPalette.borderGray),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: ColorPalette.orange),
                                        ),
                                        labelText: 'Code de vérification'),

                                    // keyboardType: TextInputType.emailAddress,
                                    validator: (code) {
                                      if (code.isEmpty)
                                        return "Le code de vérification est requis!";
                                    },
                                  ),
                                ),
//                                Padding(
//                                  padding: const EdgeInsets.only(
//                                      top: 50.0, right: 40, left: 20),
//                                  child: TextFormField(
//                                    controller: oldPasswordController,
//                                    decoration: InputDecoration(
//                                        prefixIcon: Padding(
//                                          padding: const EdgeInsets.all(8.0),
//                                          child: Icon(
//                                            Icons.lock_open,
//                                            color: ColorPalette.orange,
//                                          ),
//                                        ),
//                                        errorBorder: OutlineInputBorder(
//                                          borderSide: BorderSide(
//                                              color: ColorPalette.red),
//                                        ),
//                                        disabledBorder: OutlineInputBorder(
//                                          borderSide: BorderSide(
//                                              color: ColorPalette.borderGray),
//                                        ),
//                                        enabledBorder: OutlineInputBorder(
//                                          borderSide: BorderSide(
//                                              color: ColorPalette.borderGray),
//                                        ),
//                                        focusedBorder: OutlineInputBorder(
//                                          borderSide: BorderSide(
//                                              color: ColorPalette.orange),
//                                        ),
//                                        labelText: 'ancien mot de passe'),
//
//                                    // keyboardType: TextInputType.emailAddress,
//                                    validator: (oldPass) {
//                                      if (oldPass.isEmpty)
//                                        return "Entrez l'ancien mot de passe!";
//                                    },
//                                  ),
//                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 50.0, right: 40, left: 20),
                                  child: TextFormField(
                                    controller: newPasswordController,
                                    decoration: InputDecoration(
                                        prefixIcon: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Icon(
                                            Icons.lock_outline,
                                            color: ColorPalette.orange,
                                          ),
                                        ),
                                        errorBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: ColorPalette.red),
                                        ),
                                        disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: ColorPalette.borderGray),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: ColorPalette.borderGray),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: ColorPalette.orange),
                                        ),
                                        labelText: 'nouveau mot de passe'),

                                    // keyboardType: TextInputType.emailAddress,
                                    validator: (newPass) {
                                      if (newPass.isEmpty)
                                        return "Entrez un nouveau mot de passe!";
                                    },
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 50.0, right: 40, left: 20),
                                  child: TextFormField(
                                    controller: confirmPasswordController,
                                    decoration: InputDecoration(
                                        prefixIcon: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Icon(
                                            Icons.lock_outline,
                                            color: ColorPalette.orange,
                                          ),
                                        ),
                                        errorBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: ColorPalette.red),
                                        ),
                                        disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: ColorPalette.borderGray),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: ColorPalette.borderGray),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: ColorPalette.orange),
                                        ),
                                        labelText: 'Confirmez le mot de passe'),

                                    // keyboardType: TextInputType.emailAddress,
                                    validator: (confirmPass) {
                                      if (confirmPass.isEmpty)
                                        return "Veuillez ressaisir le mot de passe!";
                                      if (newPasswordController.text !=
                                          confirmPass)
                                        return "Le mot de passe ne correspond pas";
                                    },
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 20.0, top: 30, right: 20, bottom: 20),
                              child: MaterialButton(
                                minWidth: MediaQuery.of(context).size.width,
                                height: 50,
                                onPressed: () {
                                  if (_formKey1.currentState.validate()) {
                                    this.widget.authBloc.dispatch(
                                        ResetPasswordEvent(
                                            resetPasswordModel:
                                                ResetPasswordModel(
//                                                    oldPassword:
//                                                        oldPasswordController
//                                                            .text,
                                                    newPassword:
                                                        newPasswordController
                                                            .text),
                                            verificationCode:
                                                verificationCodeController
                                                    .text));
                                  }
                                },
                                child: Text("SOUMETTRE"),
                                textColor: Colors.white,
                                color: ColorPalette.orange,
                                shape: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                    borderSide:
                                        BorderSide(color: Colors.orange)),
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          )),
    );
  }

  void _handleState(AuthState state, BuildContext context) {
    if (state is ResetPasswordLoadingState) {
      this.onWidgetDidBuild(() => this
          .showLoaderSnackBar(context, loaderText: "Validation du Email ..."));
    } else {
      this.onWidgetDidBuild(() => this.hideLoaderSnackBar(context));
    }
    if (state is ResetPasswordFailureState) {
      this.onWidgetDidBuild(() => this.showErrorDialog(context, state.error));
    }
    if (state is ResetPasswordSuccessState) {
      this.onWidgetDidBuild(() {
        Navigator.pop(context);
        print("Email reseted avec succès");
      });
    }
  }
}
