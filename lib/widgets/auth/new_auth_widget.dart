import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pressy_client/blocs/auth/auth_bloc.dart';
import 'package:pressy_client/blocs/auth/login/login_state.dart';
import 'package:pressy_client/data/data_source/member/member_data_source.dart';
import 'package:pressy_client/data/session/member/member_session.dart';
import 'package:pressy_client/services/di/service_provider.dart';
import 'package:pressy_client/utils/style/app_theme.dart';
import 'package:pressy_client/widgets/auth/new_login_widget.dart';
import 'package:pressy_client/widgets/auth/new_sign_up_widget.dart';
import 'package:pressy_client/widgets/common/mixins/lifecycle_mixin.dart';
import 'package:pressy_client/widgets/common/mixins/loader_mixin.dart';
import 'package:pressy_client/widgets/common/widgets/background.dart';
import 'package:pressy_client/widgets/common/widgets/wavy_widget_clipper.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

class AuthWidget extends StatefulWidget with WidgetLifeCycleMixin, LoaderMixin {
  final AuthBloc authBloc;
  final IMemberSession memberSession;
  final WidgetBuilder nextWidgetBuilder;

  AuthWidget(
      {@required this.authBloc,
      this.nextWidgetBuilder,
      @required this.memberSession})
      : assert(authBloc != null),
        assert(memberSession != null);

  @override
  _AuthWidgetState createState() => _AuthWidgetState();
}

class _AuthWidgetState extends State<AuthWidget>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  _buildCard(
      {Config config,
      Color backgroundColor = Colors.transparent,
      BuildContext context}) {
    return Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height - 300,
          width: double.infinity,
          child: Card(
            elevation: 12.0,
            margin: EdgeInsets.only(right: 0.0, left: 0.0, bottom: 16.0),
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(16.0))),
            child: WaveWidget(
              config: config,
              backgroundColor: backgroundColor,
              size: Size(double.infinity, double.infinity),
              waveAmplitude: 0,
            ),
          ),
        ),
        Padding(
          padding:
              EdgeInsets.only(top: MediaQuery.of(context).size.height - 500),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Text(
              "Pressy",
              maxLines: 1,
              style: TextStyle(
                  fontFamily: "Avenir",
                  color: Colors.orange,
                  fontSize: 50,
                  letterSpacing: 5,
                  fontWeight: FontWeight.bold,
                  shadows: [Shadow(color: Colors.orange[400], blurRadius: 20)]),
            ),
          ),
        )
      ],
    );
  }

  MaskFilter _blur;

  final List<MaskFilter> _blurs = [
    null,
    MaskFilter.blur(BlurStyle.normal, 10.0),
    MaskFilter.blur(BlurStyle.inner, 10.0),
    MaskFilter.blur(BlurStyle.outer, 10.0),
    MaskFilter.blur(BlurStyle.solid, 16.0),
  ];

  int _blurIndex = 0;

  MaskFilter _nextBlur() {
    if (_blurIndex == _blurs.length - 1) {
      _blurIndex = 0;
    } else {
      _blurIndex = _blurIndex + 1;
    }
    _blur = _blurs[_blurIndex];
    return _blurs[_blurIndex];
  }

  @override
  void initState() {
    this._tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    this._tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: BlocProvider.value(
        value: this.widget.authBloc,
        child: TabBarView(
          controller: this._tabController,
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            LoginWidget(
              onSignUpClick: () => _tabController.animateTo(1),
              authBloc: this.widget.authBloc,
              onAuthCompleted: () => Navigator.pop(context),
              memberSession: this.widget.memberSession,
              memberDataSource:
                  ServiceProvider.of(context).getService<IMemberDataSource>(),
            ),
            SignUpWidget(
              memberDataSource:
                  ServiceProvider.of(context).getService<IMemberDataSource>(),
              onBackPressed: () => this._tabController.animateTo(0),
              authBloc: this.widget.authBloc,
              onAuthCompleted: () => Navigator.pop(context),
              memberSession: this.widget.memberSession,
            )
          ],
        ),
      ),
    );
  }
}
