import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pressy_client/blocs/auth/auth_bloc.dart';
import 'package:pressy_client/blocs/auth/auth_event.dart';
import 'package:pressy_client/blocs/auth/auth_state.dart';
import 'package:pressy_client/data/model/model.dart';
import 'package:pressy_client/utils/style/app_theme.dart';
import 'package:pressy_client/utils/validators/validators.dart';
import 'package:pressy_client/widgets/auth/reset_password.dart';
import 'package:email_validator/email_validator.dart';
import 'package:pressy_client/widgets/common/mixins/error_mixin.dart';
import 'package:pressy_client/widgets/common/mixins/lifecycle_mixin.dart';
import 'package:pressy_client/widgets/common/mixins/loader_mixin.dart';

class ForgotPassword extends StatefulWidget {
  final AuthBloc authBloc;

  const ForgotPassword({Key key, this.authBloc}) : super(key: key);
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword>
    with LoaderMixin, WidgetLifeCycleMixin, ErrorMixin {
  TextEditingController textEditingController = TextEditingController();
  final _formKey0 = new GlobalKey<FormState>(debugLabel: 'forgot-password');
  ImageProvider backgroundImage;
  @override
  void didChangeDependencies() async {
    backgroundImage = AssetImage('assets/confusion.png');
    await precacheImage(backgroundImage, context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: Text(
          "réinitialiser le mot de passe",
          style: TextStyle(color: ColorPalette.orange),
        ),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: ColorPalette.orange),
      ),
      body: BlocProvider.value(
        value: this.widget.authBloc,
        child: BlocListener<AuthBloc, AuthState>(
          listener: (context, state) {
            _handleState(state, context);
          },
          child: BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
//            this._handleState(state, context);
            return LayoutBuilder(builder: (context, constraints) {
              return SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                      minWidth: constraints.maxWidth,
                      minHeight: constraints.maxHeight),
                  child: IntrinsicHeight(
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 20.0),
                      child: Form(
                        key: _formKey0,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                    height: 200,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: backgroundImage,
                                        fit: BoxFit.fill,
                                      ),
                                    )),
                                Padding(
                                  padding: const EdgeInsets.only(top: 30.0),
                                  child: Text(
                                    "Mot de passe oublié?",
                                    style: Theme.of(context).textTheme.title,
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 30.0, right: 30.0, top: 20),
                                    child: Text(
                                      "Entrez votre email enregistré ci-dessous pour recevoir les instructions de réinitialisation du mot de passe.",
                                      style: Theme.of(context)
                                          .textTheme
                                          .body1
                                          .copyWith(
                                            color: ColorPalette.textGray,
                                          ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 50.0, right: 20, left: 20),
                                  child: TextFormField(
                                    controller: textEditingController,
                                    decoration: InputDecoration(
                                      prefixIcon: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Icon(
                                          Icons.email,
                                          color: ColorPalette.orange,
                                        ),
                                      ),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                          borderSide: BorderSide(
                                            color: Colors.greenAccent,
                                            width: 30,
                                          )),
                                    ),
                                    // keyboardType: TextInputType.emailAddress,
                                    validator: (email) {
                                      if (email.isEmpty)
                                        return "Email est requis!";
                                      if (!EmailValidator.validate(email))
                                        return "S'il vous plaît entrer une adresse email valide!";
                                    },
                                  ),
                                ),
                              ],
                            ),
                            Expanded(child: Container()),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 20.0, top: 8, right: 20, bottom: 20),
                              child: MaterialButton(
                                minWidth: MediaQuery.of(context).size.width,
                                height: 50,
                                onPressed: () {
                                  if (_formKey0.currentState.validate()) {
//                                    Navigator.of(context)
//                                        .push(MaterialPageRoute(
//                                            builder: (_) => ResetPassword(
//                                                  authBloc:
//                                                      this.widget.authBloc,
//                                                )));

                                    this.widget.authBloc.dispatch(
                                          ResetPasswordRequestEvent(
                                            resetPasswordRequestModel:
                                                ResetPasswordRequestModel(
                                                    email: textEditingController
                                                        .text),
                                          ),
                                        );
                                  }
                                },
                                child: Text("SOUMETTRE"),
                                textColor: Colors.white,
                                color: ColorPalette.orange,
                                shape: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                    borderSide:
                                        BorderSide(color: Colors.orange)),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              );
            });
          }),
        ),
      ),
    ));
  }

  void _handleState(AuthState state, BuildContext context) {
    if (state is ForgotPasswordLoadingState) {
      this.onWidgetDidBuild(() => this
          .showLoaderSnackBar(context, loaderText: "Validation du Email ..."));
    } else {
      this.onWidgetDidBuild(() => this.hideLoaderSnackBar(context));
    }
    if (state is ForgotPasswordFailureState) {
      this.onWidgetDidBuild(() => this.showErrorDialog(context, state.error));
    }
    if (state is ForgotPasswordSuccessState) {
      this.onWidgetDidBuild(() {
        Navigator.pop(context);
        print("Email reseted avec succès");
        Navigator.of(context).push(MaterialPageRoute(
            builder: (_) => ResetPassword(
                  authBloc: this.widget.authBloc,
                )));
      });
    }
  }
}
