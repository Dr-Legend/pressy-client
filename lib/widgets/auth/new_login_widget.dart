import 'package:animated_size_and_fade/animated_size_and_fade.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pressy_client/blocs/auth/auth_bloc.dart';
import 'package:pressy_client/blocs/auth/login/login_bloc.dart';
import 'package:pressy_client/blocs/auth/login/login_event.dart';
import 'package:pressy_client/blocs/auth/login/login_state.dart';
import 'package:pressy_client/data/data_source/data_source.dart';
import 'package:pressy_client/data/data_source/member/member_data_source.dart';
import 'package:pressy_client/data/session/member/member_session.dart';
import 'package:pressy_client/services/di/service_provider.dart';
import 'package:pressy_client/utils/style/app_theme.dart';
import 'package:pressy_client/utils/validators/validators.dart';
import 'package:pressy_client/widgets/auth/forgot_password.dart';
import 'package:pressy_client/widgets/auth/new_sign_up_widget.dart';
import 'package:pressy_client/widgets/auth/reset_password.dart';
import 'package:pressy_client/widgets/common/mixins/error_mixin.dart';
import 'package:pressy_client/widgets/common/mixins/lifecycle_mixin.dart';
import 'package:pressy_client/widgets/common/mixins/loader_mixin.dart';
import 'package:pressy_client/widgets/common/widgets/background.dart';
import 'package:pressy_client/widgets/common/widgets/input_widget.dart';

class LoginWidget extends StatefulWidget {
  final AuthBloc authBloc;
  final VoidCallback onSignUpClick;
  final IMemberSession memberSession;
  final VoidCallback onAuthCompleted;
  final IMemberDataSource memberDataSource;
  LoginWidget(
      {@required this.memberSession,
      @required this.onAuthCompleted,
      @required this.authBloc,
      @required this.memberDataSource,
      @required this.onSignUpClick})
      : assert(memberSession != null);

  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget>
    with
        TickerProviderStateMixin,
        WidgetLifeCycleMixin,
        LoaderMixin,
        ErrorMixin {
  LoginBloc _loginBloc;
  TextEditingController _emailFieldController = TextEditingController();
  TextEditingController _passwordFieldController = TextEditingController();

  bool toggle;

  @override
  void initState() {
    toggle = false;
    this._loginBloc = LoginBloc(
      authBloc: this.widget.authBloc,
      authDataSource:
          ServiceProvider.of(this.context).getService<IAuthDataSource>(),
      memberDataSource:
          ServiceProvider.of(this.context).getService<IMemberDataSource>(),
    );
    super.initState();
  }

  List<Color> orangeGradients = [
    Color(0xFFFF9844),
    Color(0xFFFE8853),
    Color(0xFFFD7267),
  ];

  @override
  void dispose() {
    _loginBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(builder: (BuildContext context) {
        return BlocProvider.value(
          value: this._loginBloc,
          child: BlocBuilder(
              bloc: this._loginBloc,
              builder: (context, LoginState state) {
                this.onWidgetDidBuild(() => this._handleState(state, context));
                return GestureDetector(
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  child: Container(
                    child: Stack(
                      fit: StackFit.loose,
                      children: <Widget>[
                        Background(),
                        Align(
                          alignment: Alignment(0.0, -0.6),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: Container(
                              child: Image.asset("assets/ic_launcher.png"),
                            ),
                          ),
                        ),
                        SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height /
                                        2.3),
                              ),
                              Form(
                                autovalidate: true,
                                onChanged: () {
                                  this._loginBloc.dispatch(LoginSubmitFormEvent(
                                        email: this._emailFieldController.text,
                                        password:
                                            this._passwordFieldController.text,
                                      ));
                                },
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    ///holds email header and inputField
                                    AnimatedSizeAndFade(
                                      child: state.toggleLoginWidget
                                          ? buildPasswordInputRow(
                                              isFormValid: state.isValid,
                                              toggleLoginWidget:
                                                  state.toggleLoginWidget)
                                          : buildEmailInputRow(
                                              isFormValid: state.isEmailValid,
                                              toggleLoginWidget:
                                                  state.toggleLoginWidget),
                                      vsync: this,
                                      fadeInCurve: ElasticInCurve(),
                                      fadeDuration: const Duration(seconds: 1),
                                      sizeDuration: const Duration(seconds: 1),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(bottom: 50),
                                    ),
//                        roundedRectButton(
//                            "Let's get Started", signInGradients, false),

                                    GestureDetector(
                                      onTap: () {
                                        this.widget.onSignUpClick();
                                      },
                                      child: roundedRectButton(
                                          "créer un compte",
                                          signUpGradients,
                                          false,
                                          ServiceProvider.of(this.context)
                                              .getService<IMemberDataSource>()),
                                    ),
                                    InkWell(
                                      onTap: () => Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ForgotPassword(
                                                    authBloc:
                                                        this.widget.authBloc,
                                                  ))),
                                      child: Text(
                                        "Mot de passe oublié",
                                        style: TextStyle(
                                            color: Color(0xFFA0A0A0),
                                            decoration:
                                                TextDecoration.underline),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }),
        );
      }),
    );
  }

  Column buildEmailInputRow(
      {bool isFormValid, @required bool toggleLoginWidget}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 40, bottom: 10),
          child: Text(
            "Email",
            style: TextStyle(fontSize: 16, color: Color(0xFF999A9A)),
          ),
        ),
        Stack(
          alignment: Alignment.bottomRight,
          children: <Widget>[
            InputWidget(
              keyboardType: TextInputType.emailAddress,
              validator: Validators.emailValidator,
              topRight: 30.0,
              bottomRight: 0.0,
              hintText: "johnDoe@example.com",
              textEditingController: this._emailFieldController,
            ),
            Padding(
                padding: EdgeInsets.only(right: 50),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Padding(
                      padding: EdgeInsets.only(top: 40),
                      child: Text(
                        'Votre email sera utilisé pour vous identifier',
                        textAlign: TextAlign.end,
                        style:
                            TextStyle(color: Color(0xFFA0A0A0), fontSize: 12),
                      ),
                    )),
                    InkWell(
                      onTap:
                          isFormValid // you need to keep the breakpoints on. SAme error, different variable
                              ? () {
                                  if (toggleLoginWidget) {
                                    _loginBloc.dispatch(
                                        ToggleLoginWidgetEvent(false));
                                  } else {
                                    _loginBloc
                                        .dispatch(ToggleLoginWidgetEvent(true));
                                  }
                                }
                              : null,
                      child: Container(
                        padding: EdgeInsets.all(10),
                        decoration: ShapeDecoration(
                          shape: CircleBorder(),
                          gradient: LinearGradient(
                              colors: isFormValid
                                  ? signInGradients
                                  : [
                                      ColorPalette.darkGray,
                                      ColorPalette.lightGray
                                    ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight),
                        ),
                        child: ImageIcon(
                          AssetImage("assets/ic_forward.png"),
                          size: 40,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ))
          ],
        ),
      ],
    );
  }

  Column buildPasswordInputRow(
      {@required bool isFormValid, @required bool toggleLoginWidget}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 40, bottom: 10),
          child: Text(
            "Password",
            style: TextStyle(fontSize: 16, color: Color(0xFF999A9A)),
          ),
        ),
        Stack(
          alignment: Alignment.bottomRight,
          children: <Widget>[
            InputWidget(
              obscureText: true,
              validator: Validators.loginPasswordValidator,
              topRight: 30.0,
              bottomRight: 0.0,
              hintText: "password",
              textEditingController: this._passwordFieldController,
            ),
            Padding(
                padding: EdgeInsets.only(right: 50),
                child: Row(
                  children: <Widget>[
                    InkWell(
                      onTap: () =>
                          _loginBloc.dispatch(ToggleLoginWidgetEvent(false)),
                      child: Container(
                        padding: EdgeInsets.all(10),
                        decoration: ShapeDecoration(
                          shape: CircleBorder(),
                          gradient: LinearGradient(
                              colors: signInGradients,
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight),
                        ),
                        child: Text(
                          "@",
                          style: TextStyle(color: Colors.white, fontSize: 30),
                        ),
                      ),
                    ),
                    Expanded(
                        child: Padding(
                      padding: EdgeInsets.only(top: 40),
                      child: Text(
                        'Entrez votre mot de passe pour continuer...',
                        textAlign: TextAlign.end,
                        style:
                            TextStyle(color: Color(0xFFA0A0A0), fontSize: 12),
                      ),
                    )),
                    InkWell(
                      onTap: isFormValid
                          ? () => this._loginBloc.dispatch(
                              LoginButtonPressedEvent(
                                  email: this._emailFieldController.text,
                                  password: this._passwordFieldController.text))
                          : null,
                      child: Container(
                        padding: EdgeInsets.all(10),
                        decoration: ShapeDecoration(
                          shape: CircleBorder(),
                          gradient: LinearGradient(
                              colors: isFormValid
                                  ? signUpGradients
                                  : [
                                      ColorPalette.lightGray,
                                      ColorPalette.lightGray
                                    ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight),
                        ),
                        child: ImageIcon(
                          AssetImage("assets/ic_forward.png"),
                          size: 40,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ))
          ],
        ),
      ],
    );
  }

  void _handleState(LoginState state, BuildContext context) {
    if (state is LoginLoadingState)
      this.showLoaderSnackBar(context);
    else
      this.hideLoaderSnackBar(context);

    if (state is LoginFailureState) this.showErrorDialog(context, state.error);

    if (state is LoginSuccessState) this._openNextWidget(context);

    if (state is LoginFailureState)
      _loginBloc.dispatch(ToggleLoginWidgetEvent(false));
  }

  void _openNextWidget(BuildContext context) {
    this.hideLoaderSnackBar(context);
    this.widget.onAuthCompleted();
  }

  Widget roundedRectButton(String title, List<Color> gradient,
      bool isEndIconVisible, IMemberDataSource memberDataSource) {
    return Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: Stack(
        alignment: Alignment(1.0, 0.0),
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width / 1.7,
            decoration: ShapeDecoration(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),
              gradient: LinearGradient(
                  colors: gradient,
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight),
            ),
            child: Text(title,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500)),
            padding: EdgeInsets.only(top: 16, bottom: 16),
          ),
          Visibility(
            visible: isEndIconVisible,
            child: Padding(
                padding: EdgeInsets.only(right: 10),
                child: ImageIcon(
                  AssetImage("assets/ic_forward.png"),
                  size: 30,
                  color: Colors.white,
                )),
          ),
        ],
      ),
    );
  }
}

const List<Color> signInGradients = [
  Color(0xFF0EDED2),
  Color(0xFF03A0FE),
];

const List<Color> signUpGradients = [
  Color(0xFFFF9945),
  Color(0xFFFc6076),
];
