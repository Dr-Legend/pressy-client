import 'package:json_annotation/json_annotation.dart';

part 'reset_password.g.dart';

@JsonSerializable()
class ResetPasswordModel {
//  final String oldPassword;
  final String newPassword;

  ResetPasswordModel({this.newPassword});

  factory ResetPasswordModel.fromJson(Map<String, dynamic> json) =>
      _$ResetPasswordModelFromJson(json);
  Map<String, dynamic> toJson() => _$ResetPasswordModelToJson(this);
}
