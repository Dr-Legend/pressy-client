import 'dart:async';

import 'package:pressy_client/data/model/model.dart';

abstract class IAuthDataSource {
  Future<AuthCredentials> login(LoginRequestModel loginRequest);
  Future<AuthCredentials> refreshCredentials(String refreshToken);
  Future<String> resetPasswordRequest(
      ResetPasswordRequestModel resetPasswordRequestModel);
  Future<void> resetPassword(
      String verificationCode, ResetPasswordModel resetPasswordModel);
}
