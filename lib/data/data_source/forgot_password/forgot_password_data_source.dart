import 'dart:async';

import 'package:pressy_client/data/model/model.dart';

abstract class IForgotPasswordDataSource {
  Future<String> resetPasswordRequest(
      ResetPasswordRequestModel resetPasswordRequestModel);
  Future<void> resetPassword(
      String verificationCode, ResetPasswordModel resetPasswordModel);
}
