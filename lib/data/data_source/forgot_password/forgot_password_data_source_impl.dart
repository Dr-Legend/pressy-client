import 'dart:async';

import 'package:pressy_client/data/data_source/base/base_data_source.dart';
import 'package:pressy_client/data/data_source/forgot_password/forgot_password_data_source.dart';
import 'package:pressy_client/data/model/auth/reset-password-request/reset_password_request.dart';
import 'package:pressy_client/data/model/auth/reset-password/reset_password.dart';
import 'package:pressy_client/data/resources/resources.dart';

class ForgotPasswordDataSourceImpl extends DataSource
    implements IForgotPasswordDataSource {
  @override
  final ApiEndpointProvider apiEndpointProvider;

  ForgotPasswordDataSourceImpl({this.apiEndpointProvider});
  @override
  Future<void> resetPassword(
      String verificationCode, ResetPasswordModel resetPasswordModel) {
    return this.handleRequest(
      endpoint: this.apiEndpointProvider.auth.resetPassword(verificationCode),
      body: resetPasswordModel?.toJson(),
    );
  }

  @override
  Future<String> resetPasswordRequest(
      ResetPasswordRequestModel resetPasswordRequestModel) {
    return this.handleRequest(
      endpoint: this.apiEndpointProvider.auth.resetPasswordRequest,
      body: resetPasswordRequestModel?.toJson(),
      responseConverter: (json) => json['code'],
    );
  }
}
