import 'package:flutter_driver/driver_extension.dart';
import 'package:pressy_client/main.dart' as app;

void main() {
  enableFlutterDriverExtension();

  app.main();
}
//To run the tests, use: flutter drive --target=test_driver/office_manager.dart
