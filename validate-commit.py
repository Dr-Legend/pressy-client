import re
import sys

regex = r"^(feat|fix|chore|ui|perf|ci|docs)(\((auth|common|order|global)(\s?,\s?(auth|common|order|global))*\))*:\s[a-zA-Z].*$"
if not re.search(regex,sys.argv[1]):
    print("The commit "+sys.argv[1]+" does not match regex pattern: " + regex)
    sys.exit(-1)
else:
    print(sys.argv[1] + " matches the regex pattern " + regex)
